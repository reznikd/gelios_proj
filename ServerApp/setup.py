import setuptools
from distutils.core import setup

setup(
    name='ServerApp',
    version='0.0.2',
    packages=setuptools.find_packages(),
    url='https://gitlab.com/reznikd/gelios_proj',
    license='MIT',
    author='Ivan',
    author_email='iv_re@uni-bremen.de',
    description='Server side of the GeliOS System',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='gmaps, gps, pollution',
    python_requires='~=3.6',
    install_requires=[
        'asgiref==3.2.10',
        'Django==3.1.1',
        'numpy==1.19.1',
        'psycopg2==2.8.5',
        'pytz==2020.1',
        'sqlparse==0.3.1',
        'utm==0.6.0',
    ],
    entry_points={
        'console_scripts': [
            'starter=manage',
        ],
    },
)
