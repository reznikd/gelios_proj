# GeliOS: ServerApp

gelios_proj

Deployment
----------
These steps should work on any Debian-relative and were tested on Jessie and Stretch

0. Install debian dependecies: ```sudo apt-get install python3-venv python3-pip python3-dev libpq-dev postgresql postgresql-contrib libxml2 libxslt1-dev libffi-dev```
1. Create gelios_proj directory: ```mkdir gelios_proj; cd gelios_proj```
2. Create a python3 virtualenv: ```python3 -m venv gelios_proj_env```
3. Activate the virtualenv: ```source gelios_proj_env/bin/activate```
4. Clone the git repo: ```git clone git@gitlab.com:reznikd/gelios_proj.git gelios_proj```
5. Install the requirements: ```cd gelios_proj/ServerApp && pip3 install -r requirements.txt```
6. Create subdirectories: ```mkdir log; mkdir media```
7. Create a local_settings.py from mysite/local_settings.py.example and change directory paths and database password 
8. Create a postgres database using the credentials in local_settings.py DATABASES:

```
sudo su - postgres
psql
create database gelios_projdb;
create user gelios_proj with password '<INSERT PASSWORD HERE from local_settings.py>';
ALTER ROLE gelios_proj SET client_encoding TO 'utf8';
ALTER ROLE gelios_proj SET default_transaction_isolation TO 'read committed';
ALTER ROLE gelios_proj SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE gelios_projdb to gelios_proj;
\q
```
9. Run djangos database migration tool: ```python3 manage.py makemigrations GeliOS_Web ``` and then ```python3 manage.py migrate```
10. Create a superuser: ```python3 manage.py createsuperuser```
11. Run the development server on localhost:8000: ```python3 manage.py runserver 8000```


Start Server:
1. ``` cd Project/gelios_proj/gelios_proj/; source ../gelios_proj_env/bin/activate; python3 manage.py runserver```
2. ```lt --port 8000 --subdomain name```