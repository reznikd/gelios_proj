// SOURCE: https://code.tutsplus.com/tutorials/submit-a-form-without-page-refresh-using-jquery--net-59
$("form").on("submit", function(e) {
    // Read the data 
    var dataString = $(this).serialize();
    // Define of ajax element
    $.ajax({
        type: "POST",
        // Send readed data to the view.py
        data: dataString,
        // on success....
        success: function(response) {
            // print response to the console 
            console.log(response);
            //append a message box, that the data was send
            $("#map").html("<div id='message' style='text-align: center;'></div>");
            $("#message")
                .html("<h2>Your data was sent</h2>")
                .append("<p>The map will be loaded soon.</p>")
                .hide()
                // load map with response data
                .fadeIn(1500, function() {
                    $("#message").append(
                        initMap(response)
                    );
                    console.log(response.ratings[0]);
                    // build a datatable with ratings under the map
                    var table = document.getElementById("table");
                    var data_table = document.getElementById("data_table");

                    table.removeChild(data_table);

                    data_table = document.createElement("TBODY");
                    data_table.setAttribute("id", "data_table");
                    table.appendChild(data_table);

                    var i = 0;
                    response.ratings.forEach(element => {
                        switch (i) {
                            case 0:
                                var color = "#5757FF";
                                break;
                            case 1:
                                var color = "#FF5757";
                                break;
                            default:
                                var color = "#57FF57";
                        }
                        var rowNode = document.createElement("tr");
                        var cellNode = document.createElement("th");
                        var textNode = document.createTextNode(++i);
                        cellNode.style.backgroundColor = color;
                        cellNode.appendChild(textNode);
                        rowNode.appendChild(cellNode);

                        element.forEach(sub_element => {
                            var cellNode = document.createElement("td");
                            var textNode = document.createTextNode(sub_element.toFixed(3));

                            cellNode.appendChild(textNode);
                            rowNode.appendChild(cellNode);
                        });
                        data_table.appendChild(rowNode);
                    });
                });
        }
    });
    e.preventDefault();
});

// function to build a google maps element, if parameter res is "null", just load the map
// if res is not "null", build a map with routes
function initMap(res = null) {
    // define a stadart map parameters
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 13,
        center: {
            lat: 53.077439,
            lng: 8.804432
        },
        mapTypeId: "terrain",
    });
    //  get elements by id
    const originInput = document.getElementById("origin-input");
    const destinationInput = document.getElementById("destination-input");
    // Add a Google Autocomplete function to this elements
    const originAutocomplete = new google.maps.places.Autocomplete(
        originInput
    );
    // take a place_id(starting point) from Autocomplete function
    originAutocomplete.setFields(["place_id"]);
    const destinationAutocomplete = new google.maps.places.Autocomplete(
        destinationInput
    );
    // take a place_id(destination) from Autocomplete function
    destinationAutocomplete.setFields(["place_id"]);
    // show markers on the map ...
    if (res != null) {
        console.log(res.points[0][0]);
        var marker1 = new google.maps.Marker({
            position: new google.maps.LatLng(res.points[0][0]),
            animation: google.maps.Animation.DROP
        });

        marker1.setMap(map);

        var infowindow1 = new google.maps.InfoWindow({
            content: "Starting point"
        });

        infowindow1.open(map, marker1);

        var marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(res.points[0].pop()),
            animation: google.maps.Animation.DROP
        });

        marker2.setMap(map);

        var infowindow2 = new google.maps.InfoWindow({
            content: "Destination"
        });

        infowindow2.open(map, marker2);
        // and build routes between this points
        for (var i = 0, len = res.points.length; i < len; i++) {
            console.log(i);
            var route_coordinates = res.points[i];
            switch (i) {
                case 0:
                    var color = "#0000FF";
                    break;
                case 1:
                    var color = "#FF0000";
                    break;
                default:
                    var color = "#00FF00";
            }
            var routePath = new google.maps.Polyline({
                path: route_coordinates,
                geodesic: true,
                strokeColor: color,
                strokeOpacity: 0.4,
                strokeWeight: 3,
            });
            routePath.setMap(map);
        };
    };
}