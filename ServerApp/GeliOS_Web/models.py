from django.db import models
from datetime import datetime
from django.utils import timezone

#Die Methodes zum Auslesen der Zeit des Servers. Diese Zeit wird dann bei der Erstelllung der Datenbankeintraegen benutzt.
now = datetime.now()

#------------------------------------
# Definition der Feldern, die in der Datenbank abgebildet werden muessen. 
# Aus dieser Definition werden in der PostgreSQL Datenbank entsprechende 
# Felder definiert. Hier wird auch der Datentyp, der Standartwert
# der Feldern bestimmt und ob die Felder leer sein koennen.
# Jede Klasse bestimmt die Tabelle in der Datenbank. 
# Die Objekte der Klassen entsprechen den Spalten in den Tabellen   
#------------------------------------

class DataTable(models.Model):
    #------------------------------------
	# Der GPS-Empfaenger(EM506) liefert die Daten in einem DDM(Degrees
	# Decimal Minutes)-Format. Um die Datenbankstruktur sauber zu halten,
	# werden schon umgerechnete Koordinaten (in DD(Decimal Degrees)-Format)
	# in die Datenbank gespeichert.
	# Die Umrechnung passiert in coordinates.py und wird da genauer erklaert.
	# Ausserdem werden hier die anderen Umweltdaten gespeichert, die
	# spaeter fuer die Auswertung der Strecken benutzt werden 
	#------------------------------------
    # Die geographische Breite(Breitengrad, englisch latitude)
    gpsCoordinates_x = models.FloatField()
    # Die geographische Laenge(Laengengrad, englisch longitude)
    gpsCoordinates_y = models.FloatField()
    # Die Zeit der Messung
    time = models.TimeField()
    # Der Geraeuschpegel
    mic_data = models.IntegerField()
    # Die Anzahl der Staubkoerner mit dem Durchmesser kleiner als 2,5 Pikometer:   
    dust_data_25 = models.FloatField()
    # Die Anzahl der Staubkoerner mit dem Durchmesser zwischen 10 und 2,5 Pikometer:
    dust_data_10 = models.FloatField()

    #Das ist die Dublettenpruefung, damit keine gleiche Messungen in der Datenbank abgespeichert werden
    class Meta:
        unique_together = (("gpsCoordinates_x", "gpsCoordinates_y", "time"))


class TelemetryTable(models.Model):
    #------------------------------------
    # Das ist die Tabelle, die fuer statistiche Zwecke gedacht ist. 
    # Anhand dieser Tabelle kann es ermittelt werden, wie viele Messungen,
    # von welchem Geraet kommen
    #------------------------------------
    # UUID des Geraetes, jede Messeinrichtung soll eine eigene UUID haben
    uuid = models.UUIDField(primary_key=True)
    # Die Zeit der letzten Messung 
    last_report_time = models.DateTimeField(default=timezone.now)
    # Die Anzahl der Messungen von dem Geraet 
    data_amount = models.IntegerField(default=1)
    # Batterieanzeige(wird erstmal nicht benutzt)
    last_reported_bat = models.FloatField()


class HistoryTable(models.Model):
    #------------------------------------
    # Das ist eine Tabelle mit allen Daten, die an den Server zugeschickt
    # worden sind. Daraus ist es moeglich, die Fehler zu ermitteln und 
    # anhand der Messdaten zu erkennen, ob alles funktioniert, wie es soll. 
    #------------------------------------
    gpsCoordinates_x = models.FloatField()
    gpsCoordinates_y = models.FloatField()
    time = models.TimeField()
    mic_data = models.IntegerField()
    dust_data_25 = models.FloatField()
    dust_data_10 = models.FloatField()
    uuid = models.UUIDField()
    last_report_time = models.DateTimeField(default=timezone.now)
    last_reported_bat = models.FloatField()
