from django.apps import AppConfig


class GeliosWebConfig(AppConfig):
    name = 'GeliOS_Web'
