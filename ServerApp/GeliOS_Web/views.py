from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from GeliOS_Web.models import DataTable, TelemetryTable, HistoryTable
from GeliOS_Web.utils.coordinates import gps
import json, requests, traceback, polyline, re 
from GeliOS_Web.utils import ratings as rat
from GeliOS_Web.utils import averages as aver
from django.http.response import JsonResponse
from datetime import datetime
# Anzahl von JSON-Feldern
NUMBER_RECEIVED_DATA = 12
# JSON Parameter, die vorhanden sein muessen
LIST_OF_NECESSARY_KEYS = [
    'pdegree',
    'pminutes',
    'pole',
    'hdegree',
    'hminutes',
    'hemisphere',
    'time',
    'mic',
    'pm25',
    'pm10',
    'uuid',
    'bat'
]
# Google Maps API Key
API_KEY = "AIzaSyAG5MsZa9rHqmwkfjP_55a0Gf9z3lWn8t8"

# die Methode fuer die Startseite. Hier werden die Informationen 
# aus der Datenbank ausgelesen und zur index.html verschickt 
def index(request):
    # Wenn von der Internetseite(index.html) eine POST-Anfrage 
    # gekommen ist, wird diese Anfrage bearbeitet. Das bedeutet,
    # dass die Internetseite schon geladen ist, und die Benutzer
    # die Anfangs- und Endposition eingegeben haben und auf 
    # den Knopf "Send" geklickt haben, um die Streckenauswertung
    # zu bekommen. 
    if request.method == 'POST':
        print(request.POST)
	# Die Anfrage wird ausgelesen...
        request_data = request.POST.dict()
        # ...und es werden die Anfangs- und Endpositionen ausgelesen, die im Webinterface eingegeben worden sind
        origin_input = request_data.get("origin_location")
        destination_input = request_data.get("destination_location")
        
        # Danach wirdmit der Hilfe von "geocode" API von Google anhand der 
        # String-Koordinaten die place_id gesucht. 
        # Die Anleitung zu der Benutzung von API URLs : https://www.geeksforgeeks.org/get-post-requests-using-python/
        # Definition von "geocode" API URL
        google_geo = "https://maps.googleapis.com/maps/api/geocode/json"

        # Definieren der Parameter, die an die API gesendet werden sollen  
        origin_par = {'address':origin_input, 'key':API_KEY}
        destination_par = {'address':destination_input, 'key':API_KEY}

        # Senden der get-Anfrage und Speichern der Antwort als Response-Objekt 
        origin_r = requests.get(url = google_geo, params = origin_par)
        destination_r = requests.get(url = google_geo, params = destination_par)

        # Extrahieren von Daten im json-Format 
        origin_data = origin_r.json()
        destination_data = destination_r.json()

        # Extrahieren von Breitengrad, Laengengrad und formatierter Adresse
        # des ersten uebereinstimmenden Orts
        origin_latitude = origin_data['results'][0]['geometry']['location']['lat']
        origin_longitude = origin_data['results'][0]['geometry']['location']['lng'] 
        origin_formatted_address = origin_data['results'][0]['formatted_address']
        # Es wird place_id des Startpunktes ausgelesen
        origin_place_id = origin_data['results'][0]['place_id']
        # Place_id wird fuer die Directions API vorbereitet in Form von *"key": value*
        origin_place_id_r = "place_id:" + origin_place_id

        destination_latitude = destination_data['results'][0]['geometry']['location']['lat'] 
        destination_longitude = destination_data['results'][0]['geometry']['location']['lng'] 
        destination_formatted_address = destination_data['results'][0]['formatted_address'] 
        # Es wird place_id des Endpunktes ausgelesen
        destination_place_id = destination_data['results'][0]['place_id']
        # Place_id wird fuer die Directions API vorbereitet in Form von *"key": value*
        destination_place_id_r = "place_id:" + destination_place_id

        # Ausgabe der Daten:
        print("Latitude:%s\nLongitude:%s\nFormatted Address:%s\nPlace ID:%s"
            %(origin_latitude, origin_longitude, origin_formatted_address, origin_place_id)) 
        print("Latitude:%s\nLongitude:%s\nFormatted Address:%s\nPlace ID:%s"
            %(destination_latitude, destination_longitude, destination_formatted_address, destination_place_id))

        # Nachdem "place_id" gefunden wurde, koennen die Strecken zwischen diesen "place_id" gefunden werden
        # Um die Strecken zu finden, wird Directions API von Google benutzt
        # Definition von directions api URL
        dir_url = "https://maps.googleapis.com/maps/api/directions/json"

        # Definition von Parametern von  directions api
        dir_par = {'origin': origin_place_id_r, 'destination': destination_place_id_r , 'key': API_KEY, 'mode': "bicycling", 'alternatives': "true"}

        # Senden der get-Anfrage und Speichern der Antwort als Response-Objekt 
        dir_r = requests.get(url = dir_url, params = dir_par)
        
        # Extrahieren von Daten im json-Format 
        dir_data = dir_r.json()

        # Es werden fuer die Steckenauswertugn die Arrays definiert     
        to_rate = []
        points = []
        
        # Fuer alle Strecken, die von Google Directions API zurueckgesendet werden, ..
        for i in range(0, len(dir_data['routes'])): 
            # werden die Polylines(die Punkte, die eine Strecke bilden) von Strecken 
            # und deren Laenge ausgelesen
            route_points = dir_data['routes'][i]['overview_polyline']['points']
            route_distance_str = dir_data['routes'][i]['legs'][0]['distance']['text']
            # es wird Polyline in Punkte dekodiert:
            p = polyline.decode(route_points)
            # und zu Array verknuepft
            points.append(p)
            # die Stercknlaende soll auch aus dem Textfeld ausgelesen werden und in 
            # "float" umgewandelt werden
            route_distance = float(route_distance_str.split(" ")[0]) 
            # es wird danach ein Tupel erstellt, der fuer die Streckeauswertugn benutzt wird.
            route_tuple = (p, route_distance)
            # letztendlich wird das Tupel zum Array hinzugefuegt
            to_rate.append(route_tuple)
        
        # Die Strecken werden danach ausgewertet
        rat_obj = rat.Ratings()
        rat_obj_pr = rat_obj.rat_paths(to_rate)

        # und die Auswertung wird an der Seite zurueckgegeben
        response = {
                'ratings': rat_obj_pr,
                'points': list(map(lambda x: list(map(lambda z: {'lat': z[0], 'lng': z[1]}, x)), points)),
            }
        return JsonResponse(response, safe = False)
    # sonst wird einfach index.html Vorlage zurueckgegeben. 
    # In diesem Fall handelt es sich um die Neuladen der Seite   
    return render(request, 'index.html')

@csrf_exempt  # ignorieren von CSRF-Token
# ------------------------------------
# Da Django uns die Moeglichkeit gibt, bei Aufrufen einer Seite(die in 
# urls.py definiert ist) eine Methode auszufuehren, koennen wir definieren,
# dass bei einer POST-Request auf diese Seite(in unserem Fall
# __ip__:__port__/geliosweb/rest) wird diese POST-Request aufgeteilt und
# die einzelne Felder werden ausgelesen, zu den Variablen zugewiesen,
# damit man spaeter einen neuen Eintrag erstellen kann.
def receive_post(request):
    # Ueberpruefung ob es ein POST-Request ist
    if request.method == 'POST':
        # es wird eine Boolean-Variable eingefuehrt, um zu pruefen, 
        # ob die Daten in der POST-Request korrekt sind
        data_creation = True
        # POST-Request wird in json format decodiert
        post_json_data = json.loads(request.body.decode("utf-8"))
        # es wird geprueft, ob JSON korrekt ist und ob alle Felder vorhanden sind
        if len(post_json_data) != NUMBER_RECEIVED_DATA \
                or not all(key in post_json_data.keys() for key in LIST_OF_NECESSARY_KEYS):
            data_creation = False
        # es wird fuer die Testzwecke in Servevkonsole ausgegeben.
        for key, value in post_json_data.items():
            print(key, "has value:", value, sep=" ")
        # Wenn alle Felder vorhanden sind...
        if data_creation:
            # wird versucht aus der DDS(Degrees Decimal Minutes)-Format zu
	        # einem DD(Decimal Degrees)-Format zu wechseln.
            try:
                pole_gps = gps(float(post_json_data['pdegree']), float(post_json_data['pminutes']), post_json_data['pole'])
                print("pole_gps: ")
                print(pole_gps)
            except:
                print("pole_gps can't be count")
            try:   
                hemisphere_gps = gps(float(post_json_data['hdegree']), float(post_json_data['hminutes']), post_json_data['hemisphere'])
                print("hemisphere_gps: ")
                print(hemisphere_gps)
            except:
                print("hemisphere_gps can't be count")
            # Die Koordinaten werden auf die 5. Nachkommastelle abgerundet
            x, y = aver.aver_gps(pole_gps, hemisphere_gps)
            # Danach wird versucht, die Daten in eine historische Tabelle(HistoryTable)
            # zu speichern. Die Felder des Objektes sollen in  models.py vordefiniert sein 
            try:
                HistoryTable.objects.create(gpsCoordinates_x=x,
                        gpsCoordinates_y=y, 
                        time=aver.aver_time(post_json_data['time']),
                        mic_data=post_json_data['mic'], 
                        dust_data_25=post_json_data['pm25'], 
                        dust_data_10=post_json_data['pm10'], 
                        uuid=post_json_data['uuid'], 
                        last_reported_bat=post_json_data['bat'])
            except Exception as e:
                print(str(e))
                traceback.print_exc()
                print("HistoryTable.object can't be created")

            # DataTable
            # Eine interne Methode zum Erstellen neues Eintrages in der Datenbank. 
            # Die Felder muessen mit den Feldern, die in models.py vordefiniert
            # sind, ueberall stimmen. Es wird die Variable genommen aus
            # dem POST-Request genommen und an die Create-Methode uebergeben
            try:
                # Es wird geprueft, ob die gemessene Koordinaten schon im System vorhanden sind
                # wenn ja, dann wird der arithmetische Mittelwert ausgerechnet(Schon vorhandener Eintrag + neuer Eintrag )/2
                found_object = DataTable.objects.filter(gpsCoordinates_x=x,
                        gpsCoordinates_y=y,
                        time=aver.aver_time(post_json_data['time']))
                print(found_object)
                if found_object:
                    datatable_object = found_object[0]
                    datatable_object.mic_data = (datatable_object.mic_data + int(post_json_data['mic'])) / 2
                    datatable_object.dust_data_25 = (datatable_object.dust_data_25 + float(post_json_data['pm25'])) / 2
                    datatable_object.dust_data_10 = (datatable_object.dust_data_10 + float(post_json_data['pm10'])) / 2
                    # das geaenderte Objekt wird gespeichert
                    datatable_object.save()
                    print("DataTable object is successfully updated: ")
                # Wenn die Koordinaten noch nicht im System vorhanden sind, wird ein neuer Eintrag erstellt
                else:
                    print(x)
                    print(y)
                    print(post_json_data)
                    print(aver.aver_time(post_json_data['time']))
                    DataTable.objects.create(gpsCoordinates_x=x,
                            gpsCoordinates_y=y,
                            time=aver.aver_time(post_json_data['time']),
                            mic_data=int(post_json_data['mic']),
                            dust_data_25=float(post_json_data['pm25']),
                            dust_data_10=float(post_json_data['pm10']))
                    print("DataTable object is successfully created")
            except Exception as e:
                print(str(e))
                print("DataTable.object can't be created")

            # TelemetryTable
            # Die Tabelle ist fuer die Geraete vorgesehen und da werden 
            # die geraetespezifischen Daten(uuid, Anzahl den Messungen, die Zeit der letzten Messsung) gespeichert   
            try:
                # Es wird geprueft, ob der Geraet mit dem UUID schon vorhanden ist            
                if TelemetryTable.objects.filter(uuid=post_json_data['uuid']).exists():
                    #wenn "ja" die Daten werden aktualisiert   
                    telemetrytable_object = TelemetryTable.objects.get(pk=post_json_data['uuid'])
                    telemetrytable_object.last_report_time = datetime.now()
                    telemetrytable_object.data_amount = telemetrytable_object.data_amount + 1
                    telemetrytable_object.last_reported_bat = post_json_data['bat']
                    # das geaenderte Objekt wird gespeichert
                    telemetrytable_object.save()
                    print("TelemetryTable object is successfully updated: ")
                # wenn "nein", wird ein neues Objekt erstellt
                else:
                    TelemetryTable.objects.create(uuid=post_json_data['uuid'], last_reported_bat=post_json_data['bat'])
                    print("TelemetryTable object is successfully created: ")
            except Exception as e:
                print(str(e))
                traceback.print_exc()
                print("TelemetryTable.object can't be created")
        else:
            print("POST data was wrong")
        # Wenn die Felder nicht mit models.py stimmen wuerden, 
        # wird kein neuen Eintrag erstellt, sondern in der Serverkonsole
        # wird eine Fehlermeldung erscheinen.
    return render(request, 'empty.html')  # Zurueck wird eine leere Seite geschickt.

