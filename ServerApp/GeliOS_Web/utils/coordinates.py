# Hier werden die GPS-Koordinaten umgerechnet 
# Da wir fuer Google Maps Darstellung den Breitengrad und den Laengengrad brauchen,
# muessen wir von DDS(Degrees Decimal Minutes)-Format zu einem DD(Decimal Degrees)-Format wechseln.
# Ausserdem muss man betrachten, in welchem Pol/welche Hemisphaere der GPS-Empfaenger sich befinden, 
# da es fuer die Umrechnung relevant ist(multiplier-Koeffizient). 
def gps(degree, minutes, pole):
    multiplier = 1
    if 'S' in pole or 'W' in pole:
        multiplier = -1
    # zuerst rechnen wir die Minuten um(Die Minuten/60)
    new_minutes = minutes / 60
    # um die neue Koordinaten zu bekommen muessen wir die neue Minuten mit dem Gradmass addieren und mit dem Faktor(1
    # oder -1) multiplizieren
    new_cordinates = multiplier * (degree + new_minutes)
    return new_cordinates
