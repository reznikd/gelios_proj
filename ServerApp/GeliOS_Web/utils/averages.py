from datetime import datetime, timedelta


##
# Counting average gps coordinates (to sync with database)
# x float: x coordinate
# y float: y coordinate
# return (float, float): average coordinates (x, y)
def aver_gps(x, y):
    return round(x, 4), round(y, 4)


##
# Counting average time (to sync with database)
# t Time: optional, time to find average
#         if nothing sent: average from now
# return Time: average Time
def aver_time(t=None, to_round=5):
    if t is None:
        time = datetime.utcnow()
    else:
        time = datetime.strptime(t, '%H:%M:%S')

    min = to_round * (time.minute % to_round // ((to_round // 2) + 1)) + to_round * (time.minute // to_round)
    if min >= 60:
        time = time.replace(minute=0) + timedelta(hours=1)
    else:
        time = time.replace(
            minute=min
        )

    time = time.replace(second=0)

    return time.strftime('%H:%M:%S')
