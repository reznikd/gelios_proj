import numpy as np
from GeliOS_Web.models import DataTable as data
from GeliOS_Web.utils import averages


class Ratings:

    def __init__(self):
        self.DUST_MAX_VALUE = 1000
        self.DUST_MIN_VALUE = 0

        self.MIC_MAX_VALUE = 4096
        self.MIC_MIN_VALUE = 0

        self.RAT_DUST = 0.8
        self.RAT_MIC = 0.2

        self.RAT_DUST_PATH = 0.4
        self.RAT_MIC_PATH = 0.1
        self.RAT_LENGTH = 0.5

    def _get_database_object(self, x, y, t=None):
        # TODO check get on unique parameters
        aver_x, aver_y = averages.aver_gps(x, y)
        aver_time = averages.aver_time(t)
        db_objects = data.objects.filter(gpsCoordinates_x=aver_x,
                gpsCoordinates_y=aver_y)
        print("x: ", aver_x, " y: ", aver_y)
        print(db_objects)
        if db_objects:
            mean_mess = {'dust_data_10': 0.0,
                    'dust_data_25': 0.0,
                    'mic_data': 0}

            for mes in db_objects:
                mean_mess['dust_data_10'] = mean_mess['dust_data_10'] + mes.dust_data_10
                mean_mess['dust_data_25'] = mean_mess['dust_data_25'] + mes.dust_data_25
                mean_mess['mic_data'] = mean_mess['mic_data'] + mes.mic_data

            mean_mess['dust_data_10'] = mean_mess['dust_data_10'] / len(db_objects)
            mean_mess['dust_data_25'] = mean_mess['dust_data_25'] / len(db_objects)
            mean_mess['mic_data'] = mean_mess['mic_data'] / len(db_objects)

        
            return mean_mess
        else:
            return {}

    ##
    # Counting the rating (percentage) of all sensors at the given
    # point and time (optional).
    # x float: x coordinate
    # y float: y coordinate
    # t Time: optional, time of collected data
    #         if nothing sent: time now
    # return (dust: f, mic: f, all: f, {10: f, 25: f})
    def rat_all_dot(self, x, y, t=None):
        db_object = self._get_database_object(x, y, t)
        if db_object:
            d10_data_p = (db_object['dust_data_10'] - self.DUST_MIN_VALUE) / (self.DUST_MAX_VALUE - self.DUST_MIN_VALUE)
            d25_data_p = (db_object['dust_data_25'] - self.DUST_MIN_VALUE) / (self.DUST_MAX_VALUE - self.DUST_MIN_VALUE)
            rat_dust = (d10_data_p + d25_data_p) / 2
            rat_mic = (db_object['mic_data'] - self.MIC_MIN_VALUE) / (self.MIC_MAX_VALUE - self.MIC_MIN_VALUE)
            return (
                rat_dust,
                rat_mic,
                rat_dust * self.RAT_DUST + rat_mic * self.RAT_MIC,
                {
                    10: d10_data_p,
                    25: d25_data_p
                }
            )
        else:
            return (
                0,
                0,
                0,
                {
                    10: 0,
                    25: 0
                }
            )

    def _rat_path(self, path, t=None):
        aver_dust = 0
        aver_mic = 0
        aver_all = 0
        data_counter = 0
        for dot in path[0]:
            dot_rating = self.rat_all_dot(dot[0], dot[1], t)
            if dot_rating[0] != 0 or dot_rating[1] != 0:
                data_counter = data_counter + 1
            aver_dust = aver_dust + dot_rating[0]
            aver_mic = aver_mic + dot_rating[1]
            aver_all = aver_all + dot_rating[2]

        aver_dust = aver_dust / len(path[0])
        aver_mic = aver_mic / len(path[0])
        aver_all = aver_all / len(path[0])

        return (
            aver_dust,
            aver_mic,
            path[1],
            aver_all * 0.5 + path[1] * self.RAT_LENGTH,
            data_counter
        )

    ##
    # Counting the ratings of given paths at the given time.
    #
    # All given sensor data will be rated with weights:
    # mic sensor: 0.1
    # dust sensor: 0.4
    # path length: 0.5
    #
    # paths [([(x: float, y: float)], length: float)]:
    #             paths to rate.
    # t Time: optional, time of collected data
    #             if nothing sent: time now
    # return [(rat_dust_path: float, rat_mic_path: float,
    #        rat_dist: float, rat_all: float, number_data: int)]:
    #             rating (0(bad)-1(good)) of all sensors
    #             for all given paths.
    def rat_paths(self, paths, t=None):
        ratings = []
        lengths_list = list(map(lambda x: x[1], paths))
        if len(lengths_list) == 1:
            ratings_length = [1]
        else:
            ratings_length = [(x - min(lengths_list)) / (max(lengths_list) - min(lengths_list)) for x in lengths_list]
        paths_with_length_ratings = list(zip(list(map(lambda x: x[0], paths)), ratings_length))
        for path in paths_with_length_ratings:
            ratings.append(self._rat_path(path, t))

        return ratings
