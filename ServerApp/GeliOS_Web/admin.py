from django.contrib import admin

# Register your models here.
from .models import DataTable, TelemetryTable, HistoryTable

admin.site.register([DataTable])
admin.site.register([TelemetryTable])
admin.site.register([HistoryTable])