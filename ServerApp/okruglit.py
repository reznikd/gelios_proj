from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from GeliOS_Web.models import DataTable, TelemetryTable, HistoryTable
from GeliOS_Web.utils.coordinates import gps
import json, requests, traceback, polyline, re 
from GeliOS_Web.utils import ratings as rat
from GeliOS_Web.utils import averages as aver
from django.http.response import JsonResponse
from datetime import datetime

def okrugl():
    all_objects = DataTable.objects.all()
    for element in all_objects:
        try:
            print("id :", element.id)
            print("old x: ", element.gpsCoordinates_x, " y: ", element.gpsCoordinates_y)
            element.gpsCoordinates_x =  round(element.gpsCoordinates_x, 4)
            element.gpsCoordinates_y = round(element.gpsCoordinates_y, 4)
            element.save()
            print("new x: ", element.gpsCoordinates_x, " y: ", element.gpsCoordinates_y, "\n")
        except:
            print("object is already tut")
            element.delete()