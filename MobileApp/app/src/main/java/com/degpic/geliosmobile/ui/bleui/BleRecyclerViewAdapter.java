package com.degpic.geliosmobile.ui.bleui;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.degpic.geliosmobile.R;
import com.degpic.geliosmobile.ui.main.MainViewModel;

import java.util.ArrayList;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * Adapter Class for RecyclerView
 */
public class BleRecyclerViewAdapter extends RecyclerView.Adapter<BleRecyclerViewAdapter.BleViewHolder> {

    /**
     * List of BLE Devices
     */
    private ArrayList<ScanResult> bleDevices;

    /**
     * ViewModel instance
     */
    private MainViewModel mainViewModel;

    /**
     * Constructor of this class
     *
     * @param devices List of BLE Devices
     * @param mainViewModel ViewModel instance
     */
    public BleRecyclerViewAdapter(ArrayList<ScanResult> devices, MainViewModel mainViewModel) {
        this.bleDevices = devices;
        this.mainViewModel = mainViewModel;
    }

    /**
     * Method creates the new BleViewHolder instance
     *
     * @param parent instance  of the parent node
     * @param viewType Number, that describes the type of view
     * @return Instance of new BlewViewHolder class
     */
    @NonNull
    @Override
    public BleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ble_device_row, parent, false);

        Log.i("RecyclerView", "OnCreateViewHolder. The Model is: " + mainViewModel);

        BleViewHolder vh = new BleViewHolder(v);
        return vh;

    }

    /**
     * Called to recycle the old row with new data
     *
     * @param holder instance of holder to be recycled
     * @param position position of new data in the list
     */
    @Override
    public void onBindViewHolder(@NonNull BleViewHolder holder, int position) {
        ScanResult btDevice = bleDevices.get(position);
        holder.getMacView().setText(btDevice.getDevice().getAddress());
        Log.i("RecyclerView", "OnBindViewHolder. The Device Name is: " + btDevice.getScanRecord().getDeviceName());
        if (btDevice.getScanRecord().getDeviceName() != null) {
            holder.getNameView().setText(btDevice.getScanRecord().getDeviceName());
        }
        Log.i("RecyclerView", "OnBindViewHolder. The Device is: " + btDevice);
        holder.setDevice(btDevice.getDevice());
    }

    /**
     * Returns count of items in list
     *
     * @return count of items in list
     */
    @Override
    public int getItemCount() {
        return this.bleDevices.size();
    }

    /**
     * Sets new BLE devices in internal list
     *
     * @param devices new BLE Devices
     */
    public void setBleDevices(ArrayList<ScanResult> devices) {
        this.bleDevices = devices;
    }

    /**
     * ViewHolder class for one element in list
     */
    public class BleViewHolder extends RecyclerView.ViewHolder {

        /**
         * TextView instance for MAC of found Device
         */
        private TextView bleMac;

        /**
         * TextView instance for name(if available) of found Device
         */
        private TextView bleName;

        /**
         * BLEDevice instance
         */
        private BluetoothDevice device;

        /**
         * Constructor of BleViewHolder class
         *
         * @param v instance of its view
         */
        public BleViewHolder(View v) {
            super(v);
            bleMac = v.findViewById(R.id.BLEUUID);
            bleName = v.findViewById(R.id.BLEName);
        }

        /**
         * Returns the instance of View for MAC Address of BLE Device, stored in
         * this instance of holder
         *
         * @return instance of View for MAC Address of BLE Device, stored in
         *         this instance of holder
         */
        public TextView getMacView() {
            return bleMac;
        }

        /**
         * Returns the instance of View for Name of BLE Device, stored in
         * this instance of holder
         *
         * @return instance of View for Name of BLE Device, stored in
         *         this instance of holder
         */
        public TextView getNameView() {
            return bleName;
        }

        /**
         * Returns the instance of BLE Device, stored in this instance of holder
         *
         * @return the instance of BLE Device, stored in this instance of holder
         */
        public void setDevice(BluetoothDevice lDevice) {
            this.device = lDevice;
            super.itemView.findViewById(R.id.ble_connect_button).setOnClickListener(view -> {
                Log.i("RecyclerViewHolder", "ViewHolder. The Device is: " + device);
                view.getRootView().findViewById(R.id.stop_communication_button).setVisibility(View.INVISIBLE);
                view.getRootView().findViewById(R.id.start_communication_button).setVisibility(View.VISIBLE);
                mainViewModel.createConnectivityWith(device);
            });
        }
    }
}
