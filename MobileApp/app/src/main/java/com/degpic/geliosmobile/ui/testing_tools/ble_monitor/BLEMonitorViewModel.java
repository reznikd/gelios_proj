package com.degpic.geliosmobile.ui.testing_tools.ble_monitor;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.degpic.geliosmobile.BleService;
import com.degpic.geliosmobile.receivers.BluetoothChangedBroadcastReceiver;
import com.degpic.geliosmobile.service_utils.BleServiceConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * ViewModel class for BleMonitor Fragment from testing tools
 */
public class BLEMonitorViewModel extends AndroidViewModel {

    /**
     * Observer object
     *
     * Observes server state
     */
    private MutableLiveData<Boolean> isServiceAlive;

    /**
     * Observer object
     *
     * Observes BLE state (enabled, disabled)
     */
    private MutableLiveData<Boolean> isBleActive;

    /**
     * Observer object
     *
     * Observes BLE state (connected, disconnected)
     */
    private MutableLiveData<BluetoothDevice> isBleConnected;

    /**
     * Observer object
     *
     * Observes incoming BLE data
     */
    private MutableLiveData<JSONObject> tableData;

    /**
     * Broadcast receiver object
     *
     * On BLE status changed
     */
    private BluetoothChangedLocalReceiver bluetoothChangedLocalReceiver;

    /**
     * Broadcast receiver object
     *
     * On data from BLE received
     */
    private BleDataReceivedBroadcastReceiver bleDataReceivedBroadcastReceiver;

    /**
     * Service connection object.
     *
     * Connects with daemon service
     */
    private BleServiceConnection serviceConnection;

    /**
     * Constuctor for the BLEMonitorViewModel class
     *
     * @param application instance of current application
     */
    public BLEMonitorViewModel(@NonNull Application application) {
        super(application);

        isServiceAlive = new MutableLiveData<>();
        isBleActive = new MutableLiveData<>();
        isBleConnected = new MutableLiveData<>();
        tableData = new MutableLiveData<>();

        serviceConnection = new BleServiceConnection();
        serviceConnection.setCallbackLiveData(isBleConnected);
        serviceConnection.setServiceAliveCallback(isServiceAlive);

        if(checkIfBLEActive()) {
            isBleActive.setValue(true);
        }
        if (checkIsServiceAlive()) {
            getApplication().bindService(new Intent(getApplication(), BleService.class),
                    serviceConnection, 0);
        }

        bluetoothChangedLocalReceiver = new BluetoothChangedLocalReceiver();
        getApplication().registerReceiver(bluetoothChangedLocalReceiver,
                new IntentFilter("com.degpic.geliosmobile.BLE_STATE_CHANGED"));

        bleDataReceivedBroadcastReceiver = new BleDataReceivedBroadcastReceiver();
        getApplication().registerReceiver(bleDataReceivedBroadcastReceiver,
                new IntentFilter("com.degpic.geliosmobile.BLE_DATA_BROADCAST"));

    }

    /**
     * Checks if Bluetooth on android device is enabled
     *
     * @return true if Bluetooth device is enabled and exists
     */
    private boolean checkIfBLEActive() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }
        return mBluetoothAdapter.isEnabled();
    }

    /**
     * Checks, if related service is alive
     *
     * @return true if service is alive
     */
    private boolean checkIsServiceAlive() {
        return BleService.isBleServiceAlive;
    }

    /**
     * Changes BLE data request delay
     *
     * @param delay delay in ms
     */
    public void changeRequestDelay(int delay) {
        serviceConnection.setDelay(delay);
    }

    /**
     * Interface between View and ViewModel for table data
     *
     * @return observer for state of daemon service
     */
    public MutableLiveData<Boolean> getServiceAliveData() {
        return isServiceAlive;
    }

    /**
     * Interface between View and ViewModel for table data
     *
     * @return observer for BLE state
     */
    public MutableLiveData<Boolean> getIsBleActiveData() {
        return isBleActive;
    }

    /**
     * Method called when related fragment destroys
     */
    public void onDestroy() {
        getApplication().unregisterReceiver(bluetoothChangedLocalReceiver);
        getApplication().unregisterReceiver(bleDataReceivedBroadcastReceiver);
        if (serviceConnection.isServiceConnected()) {
            getApplication().unbindService(serviceConnection);
        }
    }

    /**
     * Interface between View and ViewModel for table data
     *
     * @return observer for table data from BLE device
     */
    public MutableLiveData<JSONObject> getTableLiveData() {
        return tableData;
    }

    /**
     * Checks, if BLE device connected
     *
     * @return true if device is connected
     */
    public MutableLiveData<BluetoothDevice> getIsBleConnected() {
        return isBleConnected;
    }

    /**
     * Intern broadcast receiver class.
     *
     * Receives update of BLE state from outer broadcast receiver
     */
    private class BluetoothChangedLocalReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.degpic.geliosmobile.BLE_STATE_CHANGED")) {
                isBleActive.postValue(checkIfBLEActive());
            }
        }
    }

    /**
     * Intern broadcast receiver class.
     *
     * Receives update of BLE data from daemon service
     */
    private class BleDataReceivedBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.degpic.geliosmobile.BLE_DATA_BROADCAST")) {
                updateBleTableData(intent.getStringExtra("bleData"));
            }
        }
    }

    /**
     * Updates UI with new BLE Data
     *
     * @param bleData new data from BLE
     */
    private void updateBleTableData(String bleData) {
        if (bleData != null) {
            try {
                JSONObject object = new JSONObject(bleData);
                tableData.setValue(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
