/**
 *
 */
package com.degpic.geliosmobile.ui.notification_bar;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.degpic.geliosmobile.BleService;
import com.degpic.geliosmobile.R;

/**
 *
 */
public class ServiceNotificationBar {

    /**
     * Debug Tag
     */
    private String TAG = "ServiceNotificationBar";

    /**
     * Instance of BleService to interact with its functions
     */
    private final BleService mainService;

    /**
     * NotificationId. Use it to update old notification and not
     * to create the new one
     */
    private int notificationId = 6283;

    /**
     * Notification Builder
     */
    private NotificationCompat.Builder builder;

    /**
     * Notification manager. Has ability to place notification
     * in notification bar of Android
     */
    private NotificationManagerCompat notificationManager;

    /**
     * Constructor of this class. Creates the skeleton of the notification.
     *
     * @param service Instance of BleService to interact with its functions
     */
    public ServiceNotificationBar(@NonNull final BleService service) {
        mainService = service;

        Intent quitIntent = new Intent("com.degpic.geliosmobile.BLE_QUIT_ACTION");
        PendingIntent quitPendingIntent =
                PendingIntent.getBroadcast(mainService, 0, quitIntent, 0);

        builder = new NotificationCompat.Builder(service, service.getString(R.string.channel_id))
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(service.getString(R.string.service_notification_bar_title))
                //.setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .addAction(android.R.drawable.btn_default_small,
                        mainService.getString(R.string.stb_cancel_btn_text),
                        quitPendingIntent);

        Log.i(TAG, "Notification Bar is created");

        this.notificationManager = NotificationManagerCompat.from(mainService);

    }

    /**
     * Notification function when app is currently connecting to device
     *
     * @param deviceName Device name to show (example MAC)
     */
    public void onDeviceConnecting(String deviceName) {
        builder.setContentText(mainService.getString(R.string.stb_connecting_text)
                + ": " + deviceName);
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * Notification function when app is currently connecting to device
     *
     * @param deviceName Device name to show (example MAC)
     */
    public void onDeviceConnected(String deviceName) {
        builder.setContentText(mainService.getString(R.string.stb_connected_text)
                + ": " + deviceName);
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * Notification function when device was disconnected due of failure
     */
    public void onDeviceFailure() {
        builder.setContentText(mainService.getString(R.string.stb_disconnected_text));
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * Notification function when the device was disconnected
     */
    public void onDeviceDisconnected() {

    }

    /**
     * Function to create the notification bar
     */
    public void createNotificationBar() {
        mainService.startForeground(notificationId, builder.build());
    }

    /**
     * Function to remove the notification bar
     */
    public void removeNotificationBar() {
        notificationManager.cancel(notificationId);
    }

}
