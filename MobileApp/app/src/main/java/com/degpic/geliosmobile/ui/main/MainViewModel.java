package com.degpic.geliosmobile.ui.main;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.degpic.geliosmobile.BleService;
import com.degpic.geliosmobile.bluetooth.BleScanner;

import java.util.ArrayList;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * ViewModel class for MainFragment
 */
public class MainViewModel extends AndroidViewModel {

    /**
     * Debug Tag
     */
    private String TAG = "MainViewModel";

    /**
     * List of currently found BLE Devices
     */
    private MutableLiveData<ArrayList<ScanResult>> foundBleDevices;

    /**
     * Intent to launch or connect to service
     */
    private final Intent serviceConnectionIntent;

    /**
     * Instance of BLEScanner class
     */
    private final BleScanner bleScanner;

    /**
     * Constructor of this class. Called automatically bei creation of fragment class
     *
     * @param application instance of Application class
     */
    public MainViewModel(@NonNull final Application application) {
        super(application);

        foundBleDevices = new MutableLiveData<>();
        bleScanner = new BleScanner(foundBleDevices);

        serviceConnectionIntent = new Intent(application, BleService.class);

        if (isBleServiceAlive()) {
            Log.i(TAG, "BleService is still alive");
        }
    }

    /**
     * Checks availability of BLEService
     *
     * @return true if available, false else
     */
    private boolean isBleServiceAlive() {
        Log.i(TAG, "Checking if the BleService is still alive service");
        return BleService.isBleServiceAlive;
    }

    /**
     * Start scanning.
     */
    public void startScannerAndConnect() {
        bleScanner.startScan();
    }

    /**
     * Method creates connection with BLE Device nearby
     *
     * @param device Bluetooth device to connect with
     */
    public void createConnectivityWith(BluetoothDevice device) {
        Log.i(TAG, "CreateConnectivityWith: device is: " + device);
        stopRoutine();
        serviceConnectionIntent.putExtra("BleDevice", device);
        getApplication().startService(serviceConnectionIntent);
    }

    /**
     * Stops scanning
     */
    public void stopRoutine() {
        if (bleScanner.isScanning()) {
            bleScanner.stopScan();
        }
    }

    /**
     * LiveData method for DataBinding
     *
     * @return list of found BLE Devices
     */
    public LiveData<ArrayList<ScanResult>> foundDevices() {
        return foundBleDevices;
    }

    /**
     * called on Destroy the class
     */
    public void onDestroy() {
        stopRoutine();
    }
}
