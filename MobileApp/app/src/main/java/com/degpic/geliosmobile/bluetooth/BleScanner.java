package com.degpic.geliosmobile.bluetooth;

import android.os.ParcelUuid;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * BLE Scanner class. Contains implementation of callback methods of scanner.
 */
public class BleScanner extends ScanCallback {

    /**
     * Debug tag
     */
    private String TAG = "BleScanner";

    /**
     * Scan state identifier
     */
    private boolean isScanning;

    /**
     * LiveData to update UI elements
     */
    private MutableLiveData<ArrayList<ScanResult>> viewCallback;

    /**
     * Constructor of this class
     *
     * @param viewCallback LiveData to update UI elements on new Devices
     */
    public BleScanner(MutableLiveData<ArrayList<ScanResult>> viewCallback) {
        this.viewCallback = viewCallback;
        this.isScanning = false;
    }

    /**
     * Starts scan of BLE devices nearby
     */
    public void startScan() {
        Log.i(TAG, "Scan will now be started");
        BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
        ScanSettings settings = new ScanSettings.Builder()
                .setLegacy(false)
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setReportDelay(1000)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
        ParcelUuid mUuid = null; //ParcelUuid.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
        this.isScanning = true;
        scanner.startScan(filters, settings, this);
    }

    /**
     * Called automatically on find one BLE device nearby
     *
     * @param callbackType describes the type of callback
     * @param result Contains information about found device
     */
    @Override
    public void onScanResult(final int callbackType, @NonNull final ScanResult result) {
        // This method is called only if the scan report delay is not set or is set to 0.
        viewCallback.postValue(new ArrayList<>(Arrays.asList(result)));
    }

    /**
     * Called automatically on update of found devices
     *
     * @param results Contains information about found devices
     */
    @Override
    public void onBatchScanResults(@NonNull final List<ScanResult> results) {
        // This method is called only if the report delay set above is greater then 0.
        Log.i(TAG, "Multiple devices found: " + results.size());
        viewCallback.postValue(new ArrayList<>(results));
    }

    /**
     * Stops scanning mode
     */
    public void stopScan() {
        BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
        scanner.stopScan(this);
        this.isScanning = false;
    }

    /**
     * Method called to check the state of BLE scannerr
     *
     * @return true if currency scanning, false else
     */
    public boolean isScanning() {
        return isScanning;
    }
}
