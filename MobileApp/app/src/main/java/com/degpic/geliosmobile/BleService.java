package com.degpic.geliosmobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleService;

import com.degpic.geliosmobile.bluetooth.BleConnector;
import com.degpic.geliosmobile.service_utils.BleServiceBinder;
import com.degpic.geliosmobile.ui.notification_bar.ServiceNotificationBar;

/**
 * Class for background tasks.
 *
 * The background tasks like communication with ESP32 and similar, that
 * do not requires the user attention and should work after the application
 * will be closed
 */
public class BleService extends LifecycleService {

    /**
     * Intern flag for Activity
     *
     * True if BleService is online
     */
    public static boolean isBleServiceAlive = false;

    /**
     * Debug tag
     */
    private String TAG = "BleService";

    /**
     * Object of the notification bar
     */
    private ServiceNotificationBar serviceNotificationBar;

    /**
     * Object for broadcast receiver of messages from notification bar
     */
    private NotificationBarBroadcastReceiver notificationBarBroadcastReceiver;

    /**
     * Service binder object
     *
     * To connect Service to other parts of the app
     */
    private BleServiceBinder serviceBinder;

    /**
     * Bluetooth Low Energy connector object
     */
    private BleConnector connector;

    /**
     * Receiver object for the App intern broadcasts
     *
     * BLE State has changed
     */
    private BleStateChangedBroadcastReceiver bleStateChangedBroadcastReceiver;

    /**
     * Get the current connected Device
     *
     * @return current connected Device
     */
    public BluetoothDevice getConnectedDevice() {
        return connector.getBluetoothDevice();
    }

    /**
     * Changes delay between data collections
     *
     * @param delay delay in ms
     */
    public void setBleRequestDelay(int delay) {
        connector.changeDelay(delay);
    }

    /**
     * Routine, when the BLE device has connected to the smartphone
     *
     * @param device1 connected device
     */
    public void onDeviceConnected(BluetoothDevice device1) {
        serviceNotificationBar.onDeviceConnected(device1.getAddress());
        Toast.makeText(this, R.string.stb_connected_text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Broadcast receiver class
     *
     * Catches the intern broadcasts: BLE state has changed
     */
    private class BleStateChangedBroadcastReceiver extends BroadcastReceiver {

        /**
         * Will be called by catching the appropriate Broadcast
         *
         * @param context Context, that sends the broadcast
         * @param intent intent of the broadcast
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.degpic.geliosmobile.BLE_STATE_CHANGED")) {
                if (!isBleEnabled())
                    stopWithToast(R.string.toast_ble_not_enabled);
            }
        }
    }

    /**
     * Broadcast receiver class
     *
     * Catches the intern broadcasts: Broadcasts from notification bar
     */
    public class NotificationBarBroadcastReceiver extends BroadcastReceiver {

        /**
         * Debug tag
         */
        private String TAG = "NotificationBarBroadcastReceiver";

        /**
         * Will be called by catching the appropriate Broadcast
         *
         * @param context Context, that sends the broadcast
         * @param intent intent of the broadcast
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "Broadcast received");
            if (intent.getAction().equals("com.degpic.geliosmobile.BLE_QUIT_ACTION")) {
                BleService.this.stopSelf();
            }
        }
    }

    /**
     * Service binder method
     *
     * Called if some Application(other Service, Activity, etc) wants to bind this
     * service. Will be called only for first bind attempt.
     *
     * @param intent call intent
     * @return IBinder object
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        Log.i(TAG, "Service binded");
        if (serviceBinder == null)
            serviceBinder = new BleServiceBinder(this);
        return serviceBinder;
    }

    /**
     * Entry point of service.
     *
     * Called automatically during creation of the service
     */
    @Override
    public void onCreate() {
        super.onCreate();

        String uuid = (getSharedPreferences("DeviceInfo", MODE_PRIVATE)
                .getString("UUID", null));

        Log.i(TAG, "Service created");

        if (isBleEnabled()) {

            isBleServiceAlive = true;
            serviceNotificationBar = new ServiceNotificationBar(this);

            notificationBarBroadcastReceiver = new NotificationBarBroadcastReceiver();
            IntentFilter intentFilter = new IntentFilter("com.degpic.geliosmobile.BLE_QUIT_ACTION");
            registerReceiver(notificationBarBroadcastReceiver, intentFilter);

            bleStateChangedBroadcastReceiver = new BleStateChangedBroadcastReceiver();
            registerReceiver(bleStateChangedBroadcastReceiver,
                    new IntentFilter("com.degpic.geliosmobile.BLE_STATE_CHANGED"));

            serviceNotificationBar.createNotificationBar();

            connector = new BleConnector(this, uuid);
        } else {
            stopWithToast(R.string.toast_ble_not_enabled);
        }
    }

    /**
     * Data updated method.
     *
     * Sends intern broadcast so the data can be accessed from the other parts
     * of the application
     *
     * @param s BLE receive data
     */
    public void onDataUpdated(String s) {
        Intent dataBroadcast = new Intent();
        dataBroadcast.setAction("com.degpic.geliosmobile.BLE_DATA_BROADCAST");
        dataBroadcast.putExtra("bleData", s);

        sendBroadcast(dataBroadcast);
    }

    /**
     * Checks if BLE is enabled
     *
     * @return true if BLE enabled, false else
     */
    private boolean isBleEnabled() {
        BluetoothAdapter ad = BluetoothAdapter.getDefaultAdapter();
        return ad != null && ad.isEnabled();
    }

    /**
     * Stops service with notification message(through toast message)
     *
     * @param toast_ble_not_enabled id of text from values
     */
    private void stopWithToast(int toast_ble_not_enabled) {
        Toast toast = Toast.makeText(BleService.this,
                toast_ble_not_enabled,
                Toast.LENGTH_LONG);
        toast.show();

        stopSelf();
    }

    /**
     * Last method of service
     *
     * Called automatically before destroying the service
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "Service destroyed");

        connector.stopAndDisconnect();

        isBleServiceAlive = false;
        serviceNotificationBar.removeNotificationBar();
        unregisterReceiver(notificationBarBroadcastReceiver);
        unregisterReceiver(bleStateChangedBroadcastReceiver);
    }

    /**
     *
     * Called automatically every time the other part of the application
     * creates service with appropriate intent.
     *
     * @param intent caller intent
     * @param flags call flags
     * @param startId id of service
     * @return art of service to be started
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent.getParcelableExtra("BleDevice") != null) {
            Log.i(TAG, "Device, sent in intent is: " + (BluetoothDevice) intent.getParcelableExtra("BleDevice"));
            createBleConnection(intent.getParcelableExtra("BleDevice"));
        } else {
            Log.i(TAG, "No device was saved in intent");
            stopSelf();
        }

        return START_NOT_STICKY;
    }

    /**
     * Creates connection with BLE device.
     *
     * @param bt BLE Device to establish the connection with
     */
    public void createBleConnection(BluetoothDevice bt) {
        if (connector.isConnected()) {
            connector.stopAndDisconnect();
            serviceNotificationBar.onDeviceDisconnected();
        }
        serviceNotificationBar.onDeviceConnecting(bt.getAddress());
        connector.createConnectivityWith(bt);
    }

    /**
     * Called if BLE device can not be connected due of some reason
     *
     * @param device BLE Device
     * @param reason Reason
     */
    public void onDeviceConnectionFailure(BluetoothDevice device, int reason) {
        stopWithToast(R.string.fail_on_connect_with_ble);
    }
}
