package com.degpic.geliosmobile.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.degpic.geliosmobile.BleService;
import com.degpic.geliosmobile.R;
import com.degpic.geliosmobile.server_connection.BLEJSONParser;
import com.degpic.geliosmobile.server_connection.ServerConnector;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;

import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.ble.callback.profile.ProfileDataCallback;
import no.nordicsemi.android.ble.data.Data;

/**
 * Class implements the BleManagerCallbacks and ProfileDataCallback interfaces
 */
public class Esp32Callback implements  BleManagerCallbacks, ProfileDataCallback {

    /**
     * JSON parser instance
     */
    private BLEJSONParser parser;

    /**
     * Server connector instance
     */
    private ServerConnector connector;

    /**
     * BleService instance to update values
     */
    private BleService dataCallback;

    /**
     * Constructor of cuurrent class
     *
     * @param UUID UUID of mobile device
     * @param bleLiveData BleService instance to update values
     */
    public Esp32Callback(String UUID, BleService bleLiveData) {

        try {
            connector = new ServerConnector();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        dataCallback = bleLiveData;

        parser = new BLEJSONParser(UUID);
    }

    /**
     * Called when data is received from connected BLE device
     *
     * @param device BLE device that sends the data
     * @param data sent data
     */
    @Override
    public void onDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
        try {
            connector.sendRequest(parser.parseStringAndAddMeta(data.getStringValue(0)));
            dataCallback.onDataUpdated(parser.getBleJsonString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeviceConnecting(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onDeviceConnected(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onDeviceDisconnecting(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onDeviceDisconnected(@NonNull BluetoothDevice device) {

    }

    /**
     * Called on connection failures
     *
     * @param device lost link device
     */
    @Override
    public void onLinkLossOccurred(@NonNull BluetoothDevice device) {
        Intent lossIntent = new Intent("com.degpic.geliosmobile.BLE_LOSS_LINK");
        dataCallback.sendBroadcast(lossIntent);
    }

    @Override
    public void onServicesDiscovered(@NonNull BluetoothDevice device, boolean optionalServicesFound) {

    }

    @Override
    public void onDeviceReady(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingRequired(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBonded(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onBondingFailed(@NonNull BluetoothDevice device) {

    }

    @Override
    public void onError(@NonNull BluetoothDevice device, @NonNull String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported(@NonNull BluetoothDevice device) {

    }
}
