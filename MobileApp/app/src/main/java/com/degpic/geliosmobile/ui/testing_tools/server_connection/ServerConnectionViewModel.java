package com.degpic.geliosmobile.ui.testing_tools.server_connection;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

/**
 * ViewModel class for ServerConnection Fragment from testing tools
 */
public class ServerConnectionViewModel extends AndroidViewModel {

    /**
     * Constructor for the ServerConnectionViewModel class
     *
     * @param application instance of current application
     */
    public ServerConnectionViewModel(@NonNull Application application) {
        super(application);
    }
}
