package com.degpic.geliosmobile.service_utils;

import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

/**
 * Service connection class to exchange data between Application and Service
 */
public class BleServiceConnection implements ServiceConnection {

    /**
     * Debug tag
     */
    private String TAG = "BleServiceConnection";

    /**
     * Indicates connection to the service
     */
    private boolean isServiceConnected = false;

    /**
     * Binder Instance
     */
    private BleServiceBinder binder;

    /**
     * LiveData to update UI
     */
    private MutableLiveData<BluetoothDevice> bleDataCallback;

    /**
     * LiveData to update UI
     */
    private MutableLiveData<Boolean> isServiceAliveCallback;

    /**
     * Method is called if service connected
     *
     * @param componentName name of bind component
     * @param iBinder instance, that allow interaction with the service
     */
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.i(TAG, "Service connected");
        binder = (BleServiceBinder) iBinder;
        isServiceConnected = true;

        bleDataCallback.postValue(getConnectedDevice());
        isServiceAliveCallback.postValue(true);
    }

    /**
     * Method is called on disconnection of service
     *
     * @param componentName name of unbind component
     */
    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Log.i(TAG, "Service disconnected");
        binder = null;
        isServiceAliveCallback.postValue(false);
        isServiceConnected = false;
    }

    /**
     * Method called to get information about currently connected device
     *
     * @return currently connected BLE device
     */
    public BluetoothDevice getConnectedDevice() {
        if (isServiceConnected)
            return binder.getConnectedDevice();
        else
            return null;
    }

    /**
     * Update the delay of requests on ESP32 BLE device
     *
     * @param delay new delay
     */
    public void setDelay(int delay) {
        if (isServiceConnected)
            binder.setFrequency(delay);
    }

    /**
     * Sets LiveData, that informs other parts of Application about connected BLE device
     *
     * @param data new LiveData instance
     */
    public void setCallbackLiveData(MutableLiveData<BluetoothDevice> data) {
        bleDataCallback = data;
    }

    /**
     * Checks availability of the service ant its connection
     *
     * @return true if service is currently connected, false else
     */
    public boolean isServiceConnected() {
        return isServiceConnected;
    }

    /**
     * Sets LiveData, that informs other parts of Application about availability of service
     *
     * @param isServiceAlive new LiveData instance
     */
    public void setServiceAliveCallback(MutableLiveData<Boolean> isServiceAlive) {
        isServiceAliveCallback = isServiceAlive;
    }
}
