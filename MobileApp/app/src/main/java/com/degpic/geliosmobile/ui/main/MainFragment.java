package com.degpic.geliosmobile.ui.main;

import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.degpic.geliosmobile.BleService;
import com.degpic.geliosmobile.R;
import com.degpic.geliosmobile.service_utils.BleServiceBinder;
import com.degpic.geliosmobile.ui.bleui.BleRecyclerViewAdapter;

import java.util.ArrayList;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class MainFragment extends Fragment {

    /**
     * Number of request to enable the Bluetooth. Will be needed to separate different
     * requests from one application
     */
    private final int REQUEST_ENABLE_BT = 956;

    /**
     * ViewModel Instance
     */
    private MainViewModel mViewModel;

    /**
     * Class for RecyclerView (with list of found devices)
     */
    private RecyclerView bleRecyclerView;

    /**
     * Class for View: button that starts the routine
     */
    private Button startRoutineButton;

    /**
     * Class for View: button that stops the routine
     */
    private Button stopRoutineButton;

    /**
     * Adapter for RecyclerView. Controls and updates its content.
     */
    private BleRecyclerViewAdapter bleAdapter;

    /**
     * Listener for startCommunicationButton instance
     */
    private View.OnClickListener startCommunicationButtonListener = v -> {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(MainFragment.this.getActivity(),
                    R.string.no_ble_on_device_found,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if(!bluetoothAdapter.isEnabled()) {
            Intent bleStartIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            MainFragment.this.startActivityForResult(bleStartIntent, REQUEST_ENABLE_BT);
            return;
        }

        startScannerRoutine();
    };

    /**
     * First method, called automatically on creation the instance of MainFragment class.
     *
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState instance of class with last saves instance of all
     *                           elements on this view
     * @return root View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        View root = inflater.inflate(R.layout.main_fragment, container, false);

        startRoutineButton = root.findViewById(R.id.start_communication_button);
        startRoutineButton.setOnClickListener(startCommunicationButtonListener);

        stopRoutineButton = root.findViewById(R.id.stop_communication_button);
        stopRoutineButton.setOnClickListener(new StopCommunicationButtonListener());

        bleRecyclerView = (RecyclerView) root.findViewById(R.id.ble_devices_recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        bleRecyclerView.setHasFixedSize(true);

        bleRecyclerView.setLayoutManager(mLayoutManager);
        bleRecyclerView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));

        bleAdapter = new BleRecyclerViewAdapter(new ArrayList<>(), mViewModel);
        bleRecyclerView.setAdapter(bleAdapter);

        mViewModel.foundDevices().observe(this, this::resetBleDevicesList);

        return root;
    }

    /**
     * Listener class for stopCommunicationButton instance
     */
    class StopCommunicationButtonListener implements View.OnClickListener {

        /**
         * Method called, when action with view  element is occurred
         *
         * @param view View element
         */
        @Override
        public void onClick(View view) {
            stopRoutineButton.setVisibility(View.INVISIBLE);
            startRoutineButton.setVisibility(View.VISIBLE);
            mViewModel.stopRoutine();
        }
    }

    /**
     * Starts scan method
     */
    public void startScannerRoutine() {
        startRoutineButton.setVisibility(View.INVISIBLE);
        stopRoutineButton.setVisibility(View.VISIBLE);
        mViewModel.startScannerAndConnect();
    }

    /**
     * Overrides standard method
     *
     * @param savedInstanceState last saved state
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Method called, when instance will be destroyed
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewModel.onDestroy();
    }

    /**
     * Method called, when requested external activity ends.
     *
     * @param requestCode code to identify, what was requested
     * @param resultCode response
     * @param data intent of activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            this.startScannerRoutine();
        }
    }

    /**
     * Updates the device list on recyclerView
     *
     * @param devices new list of BLE devices
     */
    public void resetBleDevicesList(ArrayList<ScanResult> devices) {
        bleAdapter.setBleDevices(devices);
        bleAdapter.notifyDataSetChanged();
    }
}
