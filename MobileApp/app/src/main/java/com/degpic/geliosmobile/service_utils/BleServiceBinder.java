package com.degpic.geliosmobile.service_utils;

import android.bluetooth.BluetoothDevice;
import android.os.Binder;

import com.degpic.geliosmobile.BleService;

/**
 * Binder class. Binds application side with service
 */
public class BleServiceBinder extends Binder {

    /**
     * Instance of BleService class to interact
     */
    private BleService bleService;

    /**
     * Class constructor
     *
     * @param bleService Instance of BleService class to interact
     */
    public BleServiceBinder(BleService bleService) {
        this.bleService = bleService;
    }

    /**
     * Method to get the currently connected BLE device
     *
     * @return currently connected BLE device
     */
    public BluetoothDevice getConnectedDevice() {
        return bleService.getConnectedDevice();
    }

    /**
     * Updates amount of requests per time
     *
     * @param frequency number of requests per time
     */
    public void setFrequency(int frequency) {
        bleService.setBleRequestDelay(frequency);
    }
}
