/**
 *
 */
package com.degpic.geliosmobile.ui.testing_tools.ble_monitor;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.degpic.geliosmobile.R;

import org.json.JSONObject;

/**
 * View class for TestingTools: BLE Monitor.
 */
public class BLEMonitorFragment extends Fragment {

    private BLEMonitorViewModel bleMonitorViewModel;

    private TextView bleDeviceInfoView;

    private TextView bleStatusResultView;

    private RelativeLayout bleFrequencyLayout;

    private SeekBar frequencyBar;

    private TextView gpsXDegS;

    private TextView gpsXDegV;

    private TextView gpsXMinS;

    private TextView gpsXMinV;

    private TextView gpsXPoleS;

    private TextView gpsXPoleV;

    private TextView gpsYDegS;

    private TextView gpsYDegV;

    private TextView gpsYMinS;

    private TextView gpsYMinV;

    private TextView gpsYHemiS;

    private TextView gpsYHemiV;

    private TextView microphoneS;

    private TextView microphoneV;

    private TextView pm10S;

    private TextView pm10V;

    private TextView pm25S;

    private TextView pm25V;

    private TextView batteryS;

    private TextView batteryV;

    private LinearLayout tableHolder;

    private TextView noBleConnectionView;

    /**
     * Function called by view creation.
     *
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState State before closing this view
     * @return View root
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bleMonitorViewModel = ViewModelProviders.of(this).get(BLEMonitorViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ble_monitor, container, false);

        noBleConnectionView = root.findViewById(R.id.no_ble_connection_textView);

        bleDeviceInfoView = root.findViewById(R.id.ble_device_info);

        bleStatusResultView = root.findViewById(R.id.ble_status_result);

        gpsXDegS = root.findViewById(R.id.gpsxdeg_status_view);

        gpsXDegV = root.findViewById(R.id.gpsxdeg_value_view);

        gpsXMinS = root.findViewById(R.id.gpsxmin_status_view);

        gpsXMinV = root.findViewById(R.id.gpsxmin_value_view);

        gpsXPoleS = root.findViewById(R.id.gpsxpole_status_view);

        gpsXPoleV = root.findViewById(R.id.gpsxpole_value_view);

        gpsYDegS = root.findViewById(R.id.gpsydeg_status_view);

        gpsYDegV = root.findViewById(R.id.gpsydeg_value_view);

        gpsYMinS = root.findViewById(R.id.gpsymin_status_view);

        gpsYMinV = root.findViewById(R.id.gpsymin_value_view);

        gpsYHemiS = root.findViewById(R.id.gpsyhem_status_view);

        gpsYHemiV = root.findViewById(R.id.gpsyhem_value_view);

        microphoneS = root.findViewById(R.id.mic_status_view);

        microphoneV = root.findViewById(R.id.mic_value_view);

        pm10S = root.findViewById(R.id.pm10_status_view);

        pm10V = root.findViewById(R.id.pm10_value_view);

        pm25S = root.findViewById(R.id.pm25_status_view);

        pm25V = root.findViewById(R.id.pm25_value_view);

        batteryS = root.findViewById(R.id.bat_status_view);

        batteryV = root.findViewById(R.id.bat_value_view);

        tableHolder = root.findViewById(R.id.table_holder);

        bleFrequencyLayout = root.findViewById(R.id.ble_frequency_layout);

        frequencyBar = root.findViewById(R.id.frequencyBar);
        frequencyBar.setOnSeekBarChangeListener(new FrequencyChangedListener());

        bleMonitorViewModel.getServiceAliveData().observe(this, this::onServiceAlive);
        bleMonitorViewModel.getIsBleActiveData().observe(this, this::onBleActive);
        bleMonitorViewModel.getTableLiveData().observe(this, this::updateTableData);
        bleMonitorViewModel.getIsBleConnected().observe(this, this::bleConnected);

        return root;
    }

    /**
     * Function connection of BLE device
     *
     * @param device Connected Bluetooth device
     */
    private void bleConnected(BluetoothDevice device) {
        bleFrequencyLayout.setVisibility(View.VISIBLE);
        tableHolder.setVisibility(View.VISIBLE);
        noBleConnectionView.setVisibility(View.INVISIBLE);
        bleDeviceInfoView.setText(device.getAddress());
    }

    /**
     * Updates data of the table
     *
     * @param tableData New Data
     */
    private void updateTableData(JSONObject tableData) {
        CharSequence onlineText = getActivity().getText(R.string.parameter_status_online);
        CharSequence offlineText = getActivity().getText(R.string.parameter_status_offline);
        int onlineColor = ContextCompat.getColor(getContext(), android.R.color.holo_green_dark);
        int offlineColor = ContextCompat.getColor(getContext(), android.R.color.holo_red_dark);

        try {
            if (Float.parseFloat(tableData.getString("pdegree")) <= 0
                    && Float.parseFloat(tableData.getString("hdegree")) <= 0
                    && Float.parseFloat(tableData.getString("pminutes")) <= 0
                    && Float.parseFloat(tableData.getString("hminutes")) <= 0) {

                gpsXDegS.setText(offlineText);
                gpsXDegS.setTextColor(offlineColor);

                gpsXMinS.setText(offlineText);
                gpsXMinS.setTextColor(offlineColor);

                gpsXPoleS.setText(offlineText);
                gpsXPoleS.setTextColor(offlineColor);

                gpsYDegS.setText(offlineText);
                gpsYDegS.setTextColor(offlineColor);

                gpsYMinS.setText(offlineText);
                gpsYMinS.setTextColor(offlineColor);

                gpsYHemiS.setText(offlineText);
                gpsYHemiS.setTextColor(offlineColor);
            } else {
                gpsXDegS.setText(onlineText);
                gpsXDegS.setTextColor(onlineColor);

                gpsXMinS.setText(onlineText);
                gpsXMinS.setTextColor(onlineColor);

                gpsXPoleS.setText(onlineText);
                gpsXPoleS.setTextColor(onlineColor);

                gpsYDegS.setText(onlineText);
                gpsYDegS.setTextColor(onlineColor);

                gpsYMinS.setText(onlineText);
                gpsYMinS.setTextColor(onlineColor);

                gpsYHemiS.setText(onlineText);
                gpsYHemiS.setTextColor(onlineColor);
            }

            gpsXDegV.setText(tableData.getString("pdegree"));

            gpsXMinV.setText(tableData.getString("pminutes"));

            gpsXPoleV.setText(tableData.getString("pole"));

            gpsYDegV.setText(tableData.getString("hdegree"));

            gpsYMinV.setText(tableData.getString("hminutes"));

            gpsYHemiV.setText(tableData.getString("hemisphere"));

            if (Float.parseFloat(tableData.getString("mic")) <= 0) {
                microphoneS.setText(offlineText);
                microphoneS.setTextColor(offlineColor);
            } else {
                microphoneS.setText(onlineText);
                microphoneS.setTextColor(onlineColor);
            }

            microphoneV.setText(tableData.getString("mic"));

            if (Float.parseFloat(tableData.getString("pm10")) <= 0) {
                pm10S.setText(offlineText);
                pm10S.setTextColor(offlineColor);
            } else {
                pm10S.setText(onlineText);
                pm10S.setTextColor(onlineColor);
            }

            pm10V.setText(tableData.getString("pm10"));

            if (Float.parseFloat(tableData.getString("pm25")) <= 0) {
                pm25S.setText(offlineText);
                pm25S.setTextColor(offlineColor);
            } else {
                pm25S.setText(onlineText);
                pm25S.setTextColor(onlineColor);
            }

            pm25V.setText(tableData.getString("pm25"));

            if (Float.parseFloat(tableData.getString("bat")) <= 0) {
                batteryS.setText(offlineText);
                batteryS.setTextColor(offlineColor);
            } else {
                batteryS.setText(onlineText);
                batteryS.setTextColor(onlineColor);
            }
            batteryV.setText(tableData.getString("bat"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Overrides standard method
     *
     * @param savedInstanceState last saved state
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Checks availability of service
     *
     * @param isAlive true if available, else false
     */
    public void onServiceAlive(Boolean isAlive) {
        if (!isAlive) {
            bleFrequencyLayout.setVisibility(View.INVISIBLE);
            tableHolder.setVisibility(View.INVISIBLE);
            noBleConnectionView.setVisibility(View.VISIBLE);
            bleDeviceInfoView.setText(R.string.ble_not_connected_text);
        }
    }

    /**
     * Function called by changing the state of Bluetooth device
     *
     * @param isActive is BLE active
     */
    public void onBleActive(Boolean isActive) {
        if (isActive) {
            bleStatusResultView.setText(R.string.ble_is_active);
            bleStatusResultView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark));
        } else {
            bleStatusResultView.setText(R.string.ble_not_active_string);
            bleStatusResultView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));

            bleFrequencyLayout.setVisibility(View.INVISIBLE);
            tableHolder.setVisibility(View.INVISIBLE);
            noBleConnectionView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Called by System, if view will soon be destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        bleMonitorViewModel.onDestroy();
    }

    /**
     * Class to control the SeekBar on view
     */
    private class FrequencyChangedListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            bleMonitorViewModel.changeRequestDelay(progress * 100);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

}
