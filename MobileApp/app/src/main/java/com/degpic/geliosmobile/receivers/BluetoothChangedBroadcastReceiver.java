package com.degpic.geliosmobile.receivers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Broadcast receiver for system Broadcast. Filter on STATE_CHANGED for Bluetooth
 */
public class BluetoothChangedBroadcastReceiver extends BroadcastReceiver {

    /**
     * Receive method
     *
     * @param context context
     * @param intent extra information about broadcast
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent localIntent = new Intent("com.degpic.geliosmobile.BLE_STATE_CHANGED");

        if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            localIntent.putExtra(BluetoothAdapter.EXTRA_STATE, intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1));
            context.sendBroadcast(localIntent);
        }
    }
}
