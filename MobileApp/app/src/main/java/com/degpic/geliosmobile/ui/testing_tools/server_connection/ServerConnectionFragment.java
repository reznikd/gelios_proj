package com.degpic.geliosmobile.ui.testing_tools.server_connection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.degpic.geliosmobile.R;
import com.degpic.geliosmobile.ui.testing_tools.server_connection.listeners.ButtonClickListener;

/**
 * Fragment class for Server connection in testing tools
 */
public class ServerConnectionFragment extends Fragment {

    /**
     * Button UI object for sending the request
     */
    private Button sendRequestButton;

    /**
     * ViewModel of this class
     */
    private ServerConnectionViewModel connectionViewModel;

    /**
     * Constructor of this class.
     *
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState Instance of class that saves the last state of this class
     * @return root view
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        connectionViewModel = ViewModelProviders.of(this).get(ServerConnectionViewModel.class);
        View connectionFragmentView = inflater.inflate(R.layout.fragment_server_connection, container, false);

        sendRequestButton = connectionFragmentView.findViewById(R.id.sending_request_button);
        sendRequestButton.setOnClickListener(new ButtonClickListener());

        return connectionFragmentView;
    }

    /**
     * Rewrote method. Called when activity is created.
     *
     * @param savedInstanceState Instance of class that saves the last state of this class
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
