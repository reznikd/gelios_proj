package com.degpic.geliosmobile.server_connection;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


/**
 * Class to connecting and sending the request to the server.
 */
public class ServerConnector {

    /**
     * URL, where to send the info
     */
    private URL url;

    /**
     * Header of the request
     */
    private String header;

    /**
     * Constructor of ServerConnector class with custom URL and header
     *
     * @param url custom url of the page
     * @throws MalformedURLException on troubles by resolving the url
     */
    public ServerConnector(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    /**
     * Constructor of ServerConnector class with default URL and header
     *
     * @throws MalformedURLException on troubles by resolving the url
     */
    public ServerConnector() throws MalformedURLException {
        this("http://134.102.188.103:8888/geliosweb/rest");
    }

    /**
     * Class, that represents the asynchronous task to send the request on the server
     */
    private class ServerConnectionTask extends AsyncTask<JSONObject, Void, Integer> {

        /**
         * Method is called on calling execute method from other thread.
         *
         * @param jsonObjects object to send on the server
         * @return response
         */
        @Override
        protected Integer doInBackground(JSONObject... jsonObjects) {
            URLConnection connection;
            DataOutputStream output;

            Log.i("SERVER_CONNECTION","initialised");

            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.setDoOutput(true);


                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));

                for (JSONObject jsonObject : jsonObjects) {
                    writer.write(jsonObject.toString());
                }

                writer.flush();
                writer.close();
                os.close();
                return new Integer(conn.getResponseCode());

            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Integer(500);
        }

        /**
         * Method called after execution of doInBackground method is complete
         *
         * @param response response code
         */
        @Override
        protected void onPostExecute(Integer response) {
            super.onPostExecute(response);
            return;
        }
    }

    /**
     * Sending the request to the server.
     *
     * @param jsonObject JSON to send
     * @throws NullPointerException if no server was specified
     */
    public void sendRequest(JSONObject jsonObject) throws NullPointerException, IOException {
        Log.i("SERVER_CONNECTION", "Connection with" +
                url + "will now be established");

        ServerConnectionTask serverConnectionTask = new ServerConnectionTask();
        serverConnectionTask.execute(jsonObject);

        Log.i("SERVER_CONNECTION","Sent");
    }


    /**
     * Manual sets the new url
     *
     * @param url new url
     * @throws MalformedURLException on troubles by resolving the url
     */
    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }
}
