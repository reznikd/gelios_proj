/**
 *
 */
package com.degpic.geliosmobile.ui.testing_tools.server_connection.listeners;

import android.util.Log;
import android.view.View;

import com.degpic.geliosmobile.server_connection.BLEJSONParser;
import com.degpic.geliosmobile.server_connection.ServerConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.UUID;

/**
 * Click listener class for Button on Server connection screen
 */
public class ButtonClickListener implements View.OnClickListener {

    /**
     * Server connector instance
     */
    private ServerConnector serverConnector;

    /**
     * Constructor of the class. Creates the server connection instance
     */
    public ButtonClickListener() {
        super();

        try {
            serverConnector = new ServerConnector();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method called on action
     *
     * @param view Instance of target element
     */
    @Override
    public void onClick(View view) {
        try {
            serverConnector.sendRequest(generateDefaultJSONObject());
            Log.i("SERVER_THREAD", "out Without Errors");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates default JSON to send to the server
     *
     * @return default JSONObject instance
     */
    private JSONObject generateDefaultJSONObject() {
        String defaultJSONString = "{\"pdegree\":\"12.05\","
                                + "\"pminutes\" : \"24\","
                                + "\"pole\" : \"N\","
                                + "\"hdegree\" : \"52.02\","
                                + "\"hminutes\" : \"32.01\","
                                + "\"hemisphere\": \"W\","
                                + "\"time\": \"14:30:59\","
                                + "\"mic\" : \"23\","
                                + "\"pm25\" : \"15\","
                                + "\"pm10\" : \"25\","
                                + "\"uuid\" : " + UUID.randomUUID().toString() + ","
                                + "\"last_report_time\" : \"3d 2:20:10\","
                                + "\"data_amount\" : \"12\","
                                + "\"bat\" : \"7.5\"}";

        BLEJSONParser parser = new BLEJSONParser(UUID.randomUUID().toString());
        try {
            return parser.parseString(defaultJSONString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
