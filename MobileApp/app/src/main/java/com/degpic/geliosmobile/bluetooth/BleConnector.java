package com.degpic.geliosmobile.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.degpic.geliosmobile.BleService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.ble.PhyRequest;
import no.nordicsemi.android.ble.callback.DataReceivedCallback;
import no.nordicsemi.android.ble.callback.profile.ProfileDataCallback;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 *
 */
public class BleConnector extends BleManager<BleManagerCallbacks> {

    // Server characteristics
    final static UUID SERVICE_UUID = UUID.fromString("959009bc-b6e9-11ea-b3de-0242ac130004");
    final static UUID SECOND_CHAR  = UUID.fromString("a0ee616e-b6e9-11ea-b3de-0242ac130004");

    /**
     * Client characteristics
     */
    private BluetoothGattCharacteristic communicationCharacteristic;

    /**
     * Esp32 callback instance. All functions for esp32 interaction described there
     */
    private ProfileDataCallback esp32callback;

    /**
     * Instance of read handler class
     */
    private Handler bleReadHandler;

    /**
     * Instance of read task class
     */
    private ReadRequestTask readTask;

    /**
     * Instance of bleService class to interact with.
     */
    private BleService mainService;

    /**
     * The manager constructor.
     * <p>
     * After constructing the manager, the callbacks object must be set with
     * {@link #setGattCallbacks(BleManagerCallbacks)}.
     * <p>
     * To connect a device, call {@link #connect(BluetoothDevice)}.
     *
     * @param context the context.
     */
    public BleConnector(@NonNull BleService context,
                        @NonNull String UUID) {
        super(context);
        mainService = context;
        this.esp32callback = new Esp32Callback(UUID, context);

        bleReadHandler = new Handler();

        super.setGattCallbacks((BleManagerCallbacks) esp32callback);
    }

    /**
     * Method returns BleManagerGattCallback instance
     *
     * @return BleManagerGattCallback instance
     */
    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return new Esp32ManagerGattCallback();
    }


    /**
     * BluetoothGatt callbacks object.
     */
    private class Esp32ManagerGattCallback extends BleManagerGattCallback {

        /**
         * This method will be called when the device is connected and services are discovered.
         * You need to obtain references to the characteristics and descriptors that you will use.
         * Return true if all required services are found, false otherwise.
         *
         * @param gatt BluetoothGatt instance
         * @return true if connected, false else
         */
        @Override
        public boolean isRequiredServiceSupported(@NonNull final BluetoothGatt gatt) {
            final BluetoothGattService service = gatt.getService(SERVICE_UUID);
            for (BluetoothGattService ser : gatt.getServices()) {
                Log.i("BLE", "Service UUID: " + ser.getUuid().toString());
            }
            if (service != null) {
                communicationCharacteristic = service.getCharacteristic(SECOND_CHAR);
            }
            // Validate properties
            boolean readRequest = false;
            boolean notify = false;
            boolean writeRequest = false;
            if (communicationCharacteristic != null) {
                readRequest = (BluetoothGattCharacteristic.PROPERTY_READ) != 0;
                notify = (BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
                writeRequest = (BluetoothGattCharacteristic.PROPERTY_WRITE) != 0;
                communicationCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            }
            // Return true if all required services have been found
            //return firstCharacteristic != null &&
            Log.i("BLE", "Values are: " + readRequest + ", " +
                    notify + ", " + writeRequest + ", " + communicationCharacteristic +
                    ", " + service);
            return communicationCharacteristic != null
                    && writeRequest
                    && readRequest
                    && notify;
        }

        /**
         * If you have any optional services, allocate them here. Return true only if
         * they are found.
         *
         * @param gatt BluetoothGatt instance
         * @return true if supported, false else
         */
        @Override
        protected boolean isOptionalServiceSupported(@NonNull final BluetoothGatt gatt) {
            return super.isOptionalServiceSupported(gatt);
        }

        /**
         * Initialize your device here. Often you need to enable notifications and set required
         * MTU or write some initial data. Do it here.
         */
        @Override
        protected void initialize() {
            // You may enqueue multiple operations. A queue ensures that all operations are
            // performed one after another, but it is not required.
            beginAtomicRequestQueue()
                    .add(requestMtu(247) // Remember, GATT needs 3 bytes extra. This will allow packet size of 244 bytes.
                            .with((device, mtu) -> log(Log.INFO, "MTU set to " + mtu))
                            .fail((device, status) -> log(Log.WARN,
                                    "Requested MTU not supported: " + status)))
                    .add(setPreferredPhy(PhyRequest.PHY_LE_2M_MASK, PhyRequest.PHY_LE_2M_MASK, PhyRequest.PHY_OPTION_NO_PREFERRED)
                            .fail((device, status) -> log(Log.WARN, "Requested PHY not supported: " + status)))
                    //.add(enableNotifications(firstCharacteristic))
                    .done(device -> Log.i("BleConnector", "Target initialized"))
                    .enqueue();
            // Set a callback for your notifications. You may also use waitForNotification(...).
            // Both callbacks will be called when notification is received.
            setNotificationCallback(communicationCharacteristic).with(esp32callback);
        }

        /**
         * Method called on disconnection of the BLE device
         */
        @Override
        protected void onDeviceDisconnected() {
            communicationCharacteristic = null;
        }
    };

    /**
     * Writes out data to  BLE device
     *
     * @param data data to write
     */
    public void write(String data) {
        writeCharacteristic(communicationCharacteristic, data.getBytes())
                .done(dev -> Log.i("BLE", "Sent"))
                //.with(esp32callback)
                .enqueue();
    }

    /**
     * Sends read request to BLE device and reads its data
     */
    public void readRequest() {
        Log.i("BLEGatt", "Sending read request");
        readCharacteristic(communicationCharacteristic)
                .with(esp32callback)
                .done(device -> Log.i("BLE", "Data really sent"))
                .enqueue();
        Log.i("BLEGatt", "Request sent");
    }

    /**
     * Creates connectivity with device nearby
     *
     * @param device BLE device to establish the connection with
     */
    public void createConnectivityWith(BluetoothDevice device) {

        if (readTask == null) {
            readTask = new ReadRequestTask();
            connect(device)
                    .timeout(10000)
                    .retry(3, 100)
                    .done(device1 -> {
                        mainService.onDeviceConnected(device1);
                        readTask.run();
                    })
                    .fail((device1, reason) -> mainService.onDeviceConnectionFailure(device1, reason))
                    .enqueue();
            Log.i("BLE", "Sending data");
        }
    }

    /**
     * Stops the data exchange with connected BLE device and disconnects with it
     */
    public void stopAndDisconnect() {
        if (readTask != null) {
            bleReadHandler.removeCallbacks(readTask);
            readTask = null;
            Log.i("BLE", "Stop receiving Data");
            disconnect()
                    .timeout(10000)
                    .enqueue();
        }
    }

    /**
     * Updates the delay between read requests
     *
     * @param delay new delay in ms
     */
    public void changeDelay(int delay) {
        readTask.setDelay(delay);
    }

    /**
     * Repeatable sending of read requests task. Updates then the data and sends it to server
     */
    class ReadRequestTask implements Runnable {

        private int delay = 2000;

        /**
         * Method from Runnable to implement. Called on execution of this class.
         */
        @Override
        public void run() {
            try {
                readRequest();
            } finally {
                bleReadHandler.postDelayed(this, delay);
            }
        }

        /**
         * Updates delay between executions of this task
         *
         * @param delay new delay in ms
         */
        public void setDelay(int delay) {
            this.delay = delay;
        }
    }
}
