package com.degpic.geliosmobile.server_connection;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.UUID;

/**
 * Class for parsing and creation of JSON
 */
public class BLEJSONParser {

    /**
     * String of current JSON
     */
    private String bleJsonString;

    /**
     * JSON object of bleJsonString instance
     */
    private JSONObject jsonObject;

    /**
     * UUID of current mobile device
     */
    private UUID deviceUuid;

    /**
     * Constructor of current class
     *
     * @param deviceUuid UUID of  current mobile device
     */
    public BLEJSONParser(String deviceUuid) {
        this.deviceUuid = UUID.fromString(deviceUuid);
    }

    /**
     * Creates from String the JSONObject instance
     *
     * @param jsonString JSON as string
     * @return JSONObject
     * @throws JSONException if the string could not be converted to JSON
     */
    public JSONObject parseString(String jsonString) throws JSONException {
        String withoutBackspaceStringValue = jsonString.replace(" ", "");

        this.bleJsonString = withoutBackspaceStringValue;
        this.jsonObject = new JSONObject(jsonString);
        return this.jsonObject;
    }

    /**
     * Returns saved JDONObject
     *
     * @return saved JSONObject instance
     */
    public JSONObject getJsonObject() {
        return jsonObject;
    }

    /**
     * Returns string of saved JSON
     *
     * @return string of saved JSON
     */
    public String getBleJsonString() {
        return bleJsonString;
    }

    /**
     * Creates from String the JSONObject instance and adds mobile metadata
     *
     * @param stringValue string to convert in JSON
     * @return JSONObject of string with metadata
     * @throws JSONException if the string could not be converted to JSON
     */
    public JSONObject parseStringAndAddMeta(String stringValue) throws JSONException {
        parseString(stringValue);
        jsonObject.put("uuid", deviceUuid.toString());

        String currTime = DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());

        jsonObject.put("time", currTime);
        return jsonObject;
    }
}
