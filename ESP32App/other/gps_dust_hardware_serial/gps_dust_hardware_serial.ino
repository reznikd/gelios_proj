//--------------------------------------------------------------------
//          Hardware Serial
//--------------------------------------------------------------------
HardwareSerial GPS_Serial(1);
#define RXD1 27
#define TXD1 25

HardwareSerial Dust_Serial(2);
#define RXD2 16
#define TXD2 17

//--------------------------------------------------------------------
//          Feinstaubsensor - SDS011
//--------------------------------------------------------------------

#define DUST_LEN 9    // Laenge des Datenpakets minus Anfangsbyte
unsigned char incomingByte = 0;
// Char in den der Output des Feinstaubsensors temporaer gespeichert wird
unsigned char dust_buf[DUST_LEN];
unsigned char checksum = 0x00;
int pm_2_5 = 0;
int pm_10 = 0;
int i = 0;
bool dust = false;    //Boolean um anzugeben ob Daten gelesen wurden, oder nicht

// Command fuer Feinstaubsensor zum Ausgeben der Messdaten
static const char DATACMD[19] = {
  0xAA, // head
  0xB4, // command id
  0x04, // data byte 1
  0x00, // data byte 2 (set mode)
  0x00, // data byte 3 (sleep)
  0x00, // data byte 4
  0x00, // data byte 5
  0x00, // data byte 6
  0x00, // data byte 7
  0x00, // data byte 8
  0x00, // data byte 9
  0x00, // data byte 10
  0x00, // data byte 11
  0x00, // data byte 12
  0x00, // data byte 13
  0xFF, // data byte 14 (device id byte 1)
  0xFF, // data byte 15 (device id byte 2)
  0x02, // checksum
  0xAB  // tail
};

// Command fuer Feinstaubsensor um in Sleep-Modus zu wechseln (Ausschalten)
static const char SLEEPCMD[19] = {
  0xAA, // head
  0xB4, // command id
  0x06, // data byte 1
  0x01, // data byte 2 (set mode)
  0x00, // data byte 3 (sleep)
  0x00, // data byte 4
  0x00, // data byte 5
  0x00, // data byte 6
  0x00, // data byte 7
  0x00, // data byte 8
  0x00, // data byte 9
  0x00, // data byte 10
  0x00, // data byte 11
  0x00, // data byte 12
  0x00, // data byte 13
  0xFF, // data byte 14 (device id byte 1)
  0xFF, // data byte 15 (device id byte 2)
  0x05, // checksum
  0xAB  // tail
};

// Command fuer Feinstaubsensor um in Work-Modus zu wechseln (Einschalten)
static const char WORKCMD[19] = {
  0xAA, // head
  0xB4, // command id
  0x06, // data byte 1
  0x01, // data byte 2 (set mode)
  0x01, // data byte 3 (work)
  0x00, // data byte 4
  0x00, // data byte 5
  0x00, // data byte 6
  0x00, // data byte 7
  0x00, // data byte 8
  0x00, // data byte 9
  0x00, // data byte 10
  0x00, // data byte 11
  0x00, // data byte 12
  0x00, // data byte 13
  0xFF, // data byte 14 (device id byte 1)
  0xFF, // data byte 15 (device id byte 2)
  0x06, // checksum
  0xAB  // tail
};

// Methode zum Auslesen des Ouputs des Feinstaubsensors
void dust_read() {
  // Ein Byte vom Sensor wird gelesen
  incomingByte = Dust_Serial.read();

  // Wenn dieses Byte dem Message Header AA entspricht,...
  if (incomingByte == 0xAA) {
    // ...werden die restlichen Bytes des Datenpackets ausgelesen
    Dust_Serial.readBytes(dust_buf, DUST_LEN);
    dust_check();
  }
}

// Methode zum ueberpruefen des Start- und Endbits und der Pruefsumme
void dust_check() {
  // Start- und Endbits werden ueberprueft
  if ((dust_buf[0] == 0xC0) && (dust_buf[8] == 0xAB)) {

    checksum = 0x00;    // Checksum wird zurueckgesetzt
    // Pruefsumme wird ueberprueft
    for (i = 1; i <= 6; i++) {
      checksum = checksum + dust_buf[i];
    }
    if (checksum == dust_buf[7]) dust = true; // Daten wurden erfolgreich ausgelesen
    else Serial.println("Checksum Fehler: Checksum ist falsch");
  }
  else Serial.println("Fehler: -DUST- Anfangs- und/oder End-Byte ist falsch");
}

void dust_print() {
  // Bytes der beiden Werte werden nach der Formel aus dem Datenblatt umgewandelt
  pm_2_5 = ((dust_buf[2] * 0x100) + dust_buf[1]) / 0xA;
  pm_10 = ((dust_buf[4] * 0x100) + dust_buf[3]) / 0xA;
  // Werte werden im Seriellen Monitor ausgegeben
  Serial.print("PM2,5: ");
  Serial.print(pm_2_5);
  Serial.println(" ug/m3");
  Serial.print("PM10 : ");
  Serial.print(pm_10);
  Serial.println(" ug/m3");
  Serial.println("==================");
}

//--------------------------------------------------------------------
//          GPS-Empfaenger - EM506
//--------------------------------------------------------------------

#define GPS_LEN 67    // Laenge des Datenpakets minus Anfangsbyte und Message ID
#define GPS_ID_LEN 6  // Laenge der Message ID
// Char in den der Output des GPS-Empfaengers temporaer gespeichert wird
unsigned char gps_buf[GPS_LEN];
// Char in den die Message ID des Outputs zur ueberpruefung temporaer gespeichert wird
unsigned char gps_id_buf[GPS_ID_LEN];
//Boolean um anzugeben ob Daten gelesen wurden, oder nicht
bool gps = false;
//Boolean um anzugeben ob der GPS-Empfaenger eine Position ermitteln konnte oder nicht
bool gps_fixed = false;

unsigned char gps_incomingByte = 0;

String pdegree = "00";        //Grade des Breitengrads
String pminutes = "00.0000";  //Minuten des Breitengrads
String pole = "N";            //Pol des Breitengrads
String hdegree = "000";       //Grade des Laengengrads
String hminutes = "00.0000";  //Minuten des Laengengrads
String hemi = "W";            //Grade des Laengengrads
String sat = "00";            //Anzahl der gefundenen Satelliten

// Methode zum Erkennen der Message ID
void gps_checkid() {
  // Ein Byte vom Sensor wird gelesen
  gps_incomingByte = GPS_Serial.read();
  //Serial.write(incomingByte);

  // Wenn dieses Byte dem Message Header $ entspricht,...
  if (gps_incomingByte == 0x24) {
    // ...werden die Bytes der Message ID ausgelesen
    GPS_Serial.readBytes(gps_buf, GPS_ID_LEN);

    // ueberpruefung auf den GGA Header
    if (
      gps_buf[2] == 0x47 &&
      gps_buf[3] == 0x47 &&
      gps_buf[4] == 0x41 )
    {
      // Restliche Bytes des Datenpakets werden ausgelesen
      GPS_Serial.readBytes(gps_buf, GPS_LEN);
      gps = true;   // Daten wurden gelesen
      //Serial.println("gps data readed");
    }
    // Nach Bedarf kann noch nach anderen Headern
    // ueberprueft werden um sie dann auszugeben
  }
}

// Methode zum ueberpruefen ob ein GPS Signal gefunden wurde
void gps_check() {
  gps = false;    // Boolean wird zurueckgesetzt
  // Wenn der Empfaenger eine Position hat, wird der boolean auf true gesetzt
  if (gps_buf[36] == 0x31 || gps_buf[36] == 0x32) gps_fixed = true;
  else {
    gps_fixed = false;
  }
  //Serial.println("gps check");
  gps_gga_print(); // Daten werden ausgegeben
}

// Methode zum ausgeben der GGA Daten im Seriellen Monitor
void gps_gga_print() {
  Serial.println("");
  Serial.println("==================");
  Serial.println("");

  // Uhrzeit wird ausgegeben und richtig formatiert, falls Uhrzeit existiert
  if (gps_buf[0] != 0x2c) {
    for (i = 0; i < 6; i++) {
      Serial.write(gps_buf[i]);
      if (i == 1 || i == 3) Serial.print(":");
    }
    Serial.println(" UTC");
  } else {
    Serial.println("Fehler: Keine Uhrzeit gefunden oder fehlerhaft");
  }

  // Koordinaten werden nur ausgegeben, wenn eine Position gefunden wurde
  if (gps_fixed) {
    // Ausgelesene Daten werden aus dem Char-Array in Strings gespeichert
    for (i = 11; i <= 39; i++) {
      if (i == 11 || i == 12) pdegree.setCharAt(i - 11, gps_buf[i]);
      if (i >= 13 && i <= 19) pminutes.setCharAt(i - 13, gps_buf[i]);
      if (i == 21) pole.setCharAt(0, gps_buf[i]);
      if (i >= 23 && i <= 25) hdegree.setCharAt(i - 23, gps_buf[i]);
      if (i >= 26 && i <= 32) hminutes.setCharAt(i - 26, gps_buf[i]);
      if (i == 34) hemi.setCharAt(0, gps_buf[i]);
      if (i == 38 || i == 39) sat.setCharAt(i - 38, gps_buf[i]);
    }

    // Ausgelesenen Koordinaten werden im Seriellen Monitor ausgegeben
    Serial.print(pdegree);
    Serial.print(" ");
    Serial.print(pminutes);
    Serial.print(" ");
    Serial.print(pole);
    Serial.print(" ");
    Serial.print(hdegree);
    Serial.print(" ");
    Serial.print(hminutes);
    Serial.print(" ");
    Serial.println(hemi);

    // Anzahl der vom GPS-Empfaenger gefundenen GPS-Satelliten
    // wird im Seriellen Monitor ausgegeben
    Serial.print("Satelliten: ");
    Serial.println(sat);

  }
  else {
    Serial.println("Fehler: Position kann nicht bestimmt werden");
  }
}

//--------------------------------------------------------------------
//          Batterie
//--------------------------------------------------------------------

float battery_r = 0.0;  // Die Spannung der Batterie
bool battery = false;   // Boolean ob der Wert der Batterie ausgelesen wurde

// Methode zum Messen der Spannung der Batterie
void batteryCharge() {
  //hier wird dann die Batterie Spannung gemessen
  battery = true;
}

//--------------------------------------------------------------------
//          Loudness(Groove Loudness Sensor)
//--------------------------------------------------------------------

int loudness; //value of loudness sensor
float loudness_for = 0;

void loudness_measure() {
  for (int i = 0; i < 51; i++) {
    loudness_for += analogRead(36);
  }
  loudness = loudness_for / 50;
  Serial.print("loudness: ");
  Serial.println(loudness);
  loudness_for = 0;
}

//--------------------------------------------------------------------
//          Bluetooth
//--------------------------------------------------------------------

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint32_t value = 0;

#define SERVICE_UUID        "959009bc-b6e9-11ea-b3de-0242ac130004"
#define CHARACTERISTIC_UUID "a0ee616e-b6e9-11ea-b3de-0242ac130004"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    }

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};


//--------------------------------------------------------------------
//          Hier beginnt das Programm
//--------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  GPS_Serial.begin(4800, SERIAL_8N1, RXD1, TXD1);
  Serial.println("Serial Txd1 is on pin: " + String(TXD1));
  Serial.println("Serial Rxd1 is on pin: " + String(RXD1));
  Dust_Serial.begin(9600, SERIAL_8N1, RXD2, TXD2);
  Serial.println("Serial Txd2 is on pin: " + String(TXD2));
  Serial.println("Serial Rxd2 is on pin: " + String(RXD2));

  //----------------
  // BLE
  //----------------

  // Create the BLE Device
  BLEDevice::init("GELIOS");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY 
                    );



  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
  Serial.println(SERVICE_UUID);
  Serial.println(CHARACTERISTIC_UUID);

}

void loop() {
  //Batteriespannung auslesen
  //Serial.println(battery);
  if (!battery) {
    Serial.println("");

    //Multiplexer ansteuern
    Dust_Serial.flush();
    for (uint8_t i = 0; i < 19; i++) {
      Dust_Serial.write(WORKCMD[i]);
    }
    Serial.println("workcmd");
    batteryCharge();

  }

  //Messdaten des Feinstaubsensors auslesen
  if (!dust && !gps ) {
    Dust_Serial.flush();
    for (uint8_t i = 0; i < 19; i++) {
      Dust_Serial.write(DATACMD[i]);
    }
  }

  if (!dust && !gps ) {
    if (Dust_Serial.available() > 0) {
      dust_read();
      if (dust) {
        Serial.println("Dust data ready");

        //for (uint8_t i = 0; i < 19; i++) {
        //  Dust_Serial.write(SLEEPCMD[i]);
        //}

        //Multiplexer ansteuern
        GPS_Serial.flush();
      }
    }
  }
  //Position auslesen
  if (!gps && dust) {
    if (GPS_Serial.available() > 0) {
      gps_checkid();
      if (gps) {
        Serial.println("GPS data ready");
      }
    }
  }
  //Messdaten an den Server senden und ESP 32 in Schlafmodus schicken
  if (dust && gps) {
    //Print in Serial Monitor
    gps_check();
    dust_print();
    loudness_measure();

    dust = false;
    String json_data = "{\"pdegree\":" + String(" \" ") + String(pdegree) + String(" \", ") +
                        "\"pminutes\":" + String(" \" ") + String(pminutes) + String(" \", ") +
                        "\"pole\":" + String(" \" ") + String(pole) + String(" \", ") +
                        "\"hminutes\":" + String(" \" ") + String(hminutes) + String(" \", ") +
                        "\"hdegree\":" + String(" \" ") + String(hdegree) + String(" \", ") +
                        "\"hemisphere\":" + String(" \" ") + String(hemi) + String(" \", ") +
                        "\"sat\":" + String(" \" ") + String(sat) + String(" \", ") +
                        "\"pm25\":" + String(" \" ") + String(pm_2_5) + String(" \", ") +
                        "\"pm10\":" + String(" \" ") + String(pm_10) + String(" \", ") +
                        "\"bat\":" + String(" \" ") + String(battery_r) + String(" \", ") +
                        "\"mic\":" + String(" \" ") + String(loudness) + String(" \" ") +
                        String("}");
    Serial.println(json_data);

    // notify changed value
    if (deviceConnected) {
      Serial.println("connected");
      pCharacteristic->setValue(json_data.c_str());
      pCharacteristic->notify();
      delay(10); // bluetooth stack will go into congestion, if too many packets are sent, in 6 hours test i was able to go as low as 3ms
    }
    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
      delay(500); // give the bluetooth stack the chance to get things ready
      pServer->startAdvertising(); // restart advertising
      Serial.println("start advertising");
      oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
      // do stuff here on connecting
      oldDeviceConnected = deviceConnected;
    }

    //battery = false;

    //ESP8266 in Schlafmodus schicken
    //Serial.println("ESP8266 in sleep mode.");
    //ESP.deepSleep(sleepSec * 1000000);
  }
}
