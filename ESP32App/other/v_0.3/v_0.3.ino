/**
 * ESP32 sketch for read an important environment data,
 * like a GPS position(GPS sensor - EM506), 
 * particulate matter pollution(DUST sensor - SDS011), 
 * loudness(Groove loudness sensor) and send it 
 * via Bluetooth(BLE onboard device) to the Android phone.
 * 
 * Author: Ivan Reichert <iv_re@uni-bremen.de>
 * Date: autumn 2020
 */ 

//========================================================
//  ESP32 Settings
//========================================================

/**  Hardware Serial
 * -------------------------------------------------------
 * Hardware Serial(UART) is used for communication between 
 * ESP32 Board and other devices(GPS and DUST(particle 
 * concentration ) sensors). 
*/

HardwareSerial GPS_Serial(1);
#define RXD1 27  // tx from gps
#define TXD1 25  // rx from gps

HardwareSerial Dust_Serial(2);
#define RXD2 16  // tx from dust
#define TXD2 17  // rx from dust

/**  ESP32 sleep and wake up
 * -------------------------------------------------------
 * After 800 reconnect attemps(approx. 30 seconds), 
 * ESP32 goes into sleep mode. Therefore, we decide to use 
 * an external wake-up source(button) to wake up ESP32,
 * when we need it and want to reconnect. 
 * ESP32 reacts to the external pin(GPIO33), which will be
 * controlled by the button, which is connected to the 
 * 3.3V source. So, if there is HIGH on the GPIO pin 33 
 * and ESP32 is sleeping now, then ESP32 will be waked up.
*/

int bootCount = 0;           // it counts, how many times esp32 was booted
#define PIN_WAKE_UP_ESP32 33 // GPIO33, pin which connected to the button(3.3V)

/**  MOSFET control pins
 * -------------------------------------------------------
 * For power optimisation GPS(EM506) and DUST(SDS011) will
 * be controlled by ESP32 and goes "ON" only if Bluetooth
 * device is connected. For this we use 2 MOSFETs, which are
 * conntroled by the GPIO pins 5 and 18. 
*/


#define PIN_CONTROL_DUST 5 // GPIO5, control pin for DUST sensor
#define PIN_CONTROL_GPS 18 // GPIO18, control pin for GPS sensor

/**  RGB indicator LED
 * -------------------------------------------------------
 * RGB LED shows a Bluetooth state. WE use:
 * LtBlue(255,255,125) to indicate, that bluetooth is 
 * ready to cennect
 * Green(0,255,0) to indicate, that bluetooth is connected
 * RED(255,0,0) to indicate, that bluetooth connection lost
 * NONE to indicate, that ESP32 is "sleeping" now. 
 * ESP32 doesn't have a "analogWrite" function, 
 * that we to control MOSFETs, 
 * so we need to include it.
*/
#include <analogWrite.h>

#define PIN_RGB_RED 4      // GPIO4
#define PIN_RGB_GREEN 2    // GPIO2
#define PIN_RGB_BLUE 15    // GPIO15

/**  rgb_write
 * -------------------------------------------------------
 * Overview: Function to set a color on RGB LED 
 * 
 * param: 
 * int red_value - red LED value(from 0 to 255)
 * int green_value - green LED value(from 0 to 255)
 * int blue_value - blue LED value(from 0 to 255)
 * where 255 is max. light power.
 */

void rgb_write(int red_value, int green_value, int blue_value) {
  analogWrite(PIN_RGB_RED, red_value);
  analogWrite(PIN_RGB_GREEN, green_value);
  analogWrite(PIN_RGB_BLUE, blue_value);
}

/**  Battery
 * -------------------------------------------------------
 * Now is it powered by a power bank and don't need to be 
 * measured. 
*/

float battery_r = 0.0;   // battery voltage

/**  Loudness(Groove Loudness Sensor)
 * -------------------------------------------------------
 * Loudness level will be measured by a Groove loudness 
 * Sensor, which is connected to the analog pin on ESP32. 
*/

int loudness;            // average value of loudness sensor
int loudness_for = 0;    // variable to save a measured data
#define PIN_LOUDNESS 36  // GPIO36

/**  loudness_measure
 * -------------------------------------------------------
 * Overview: Function to collect a measured data(50 values)
 * for a certain period of time. After that the average value 
 * will be calculated.
 */

void loudness_measure() {
  for (int i = 0; i < 51; i++) {
    loudness_for += analogRead(PIN_LOUDNESS);
  }
  loudness = loudness_for / 50; // average value 
  loudness_for = 0;
}

//========================================================
//  DUST Sensor - SDS011
//========================================================

/**  Commands for a DUST Sensor - SDS011
 * -------------------------------------------------------
 * Functional commands to control a DUST Sensor 
 */
  
// Command for DUST Sensor to output the measurement data
static const char DATACMD[19] = {
  0xAA, // head
  0xB4, // command id
  0x04, // data byte 1
  0x00, // data byte 2 (set mode)
  0x00, // data byte 3 (sleep)
  0x00, // data byte 4
  0x00, // data byte 5
  0x00, // data byte 6
  0x00, // data byte 7
  0x00, // data byte 8
  0x00, // data byte 9
  0x00, // data byte 10
  0x00, // data byte 11
  0x00, // data byte 12
  0x00, // data byte 13
  0xFF, // data byte 14 (device id byte 1)
  0xFF, // data byte 15 (device id byte 2)
  0x02, // checksum
  0xAB  // tail
};

// Command for DUST Sensor to start to work
static const char WORKCMD[19] = {
  0xAA, // head
  0xB4, // command id
  0x06, // data byte 1
  0x01, // data byte 2 (set mode)
  0x01, // data byte 3 (work)
  0x00, // data byte 4
  0x00, // data byte 5
  0x00, // data byte 6
  0x00, // data byte 7
  0x00, // data byte 8
  0x00, // data byte 9
  0x00, // data byte 10
  0x00, // data byte 11
  0x00, // data byte 12
  0x00, // data byte 13
  0xFF, // data byte 14 (device id byte 1)
  0xFF, // data byte 15 (device id byte 2)
  0x06, // checksum
  0xAB  // tail
};


#define DUST_LEN 9    // Length of the data packet minus the start byte
unsigned char incomingByte = 0;// Char to save a temporary dust data
unsigned char dust_buf[DUST_LEN];
unsigned char checksum = 0x00;
int pm_2_5 = 0;
int pm_10 = 0;
int i = 0;
bool dust = false;    // Boolean to indicate whether data has been read or not
int sleep_counter = 0; //counter for a sleep state
bool dust_sleep_state = false; // DUST Sensor indicate state

/**  dust_send_work_cmd
 * -------------------------------------------------------
 * Overview: Function to send a WORKCMD to dust sensor.
 */

void dust_send_work_cmd() {
  // bitwise forwarding of command and wait until it is flushed
  for (int i = 0; i < 19; i++) {
    Dust_Serial.write(WORKCMD[i]);
    Dust_Serial.flush();  // It pauses the program while the transmit buffer is flushed.
  }
  Serial.println("DUST WORKCMD sent");
  sleep_counter = 0;
  dust_sleep_state = false;
}

/**  dust_read_data
 * -------------------------------------------------------
 * Overview: Function to read the output of the dust 
 * sensor, checking the startbit, endbit and the checksum
 */

void dust_read_data() {
  // A byte from the sensor is read
  incomingByte = Dust_Serial.read();

  // If this byte corresponds to the message header AA, ...
  if (incomingByte == 0xAA) {
    // ... the remaining bytes of the data packet are read out
    Dust_Serial.readBytes(dust_buf, DUST_LEN);
    // Start and end bits are checked
    if ((dust_buf[0] == 0xC0) && (dust_buf[8] == 0xAB)) {
      checksum = 0x00;    // Reset checksum
      // Check of checksum
      for (int i = 1; i <= 6; i++) {
        checksum = checksum + dust_buf[i];
      }
      if (checksum == dust_buf[7]) {
        dust = true; // Data was read out successfully
        Serial.println("Dust data ready");
      }
      else Serial.println("Checksum Fehler: Checksum is wrong");
    }
    else Serial.println("Error: -DUST- start and / or end byte is wrong");
  }
}

/**  dust_print
 * -------------------------------------------------------
 * Overview: Function to print a mesured DUST data in the 
 * serial monitor
 */

void dust_print() {
  // Bytes of the two values are converted according to
  // the formula from the data sheet
  pm_2_5 = ((dust_buf[2] * 0x100) + dust_buf[1]) / 0xA;
  pm_10 = ((dust_buf[4] * 0x100) + dust_buf[3]) / 0xA;
  // Print values in a Serial monitor
  Serial.print("PM2,5: ");
  Serial.print(pm_2_5);
  Serial.println(" ug/m3");
  Serial.print("PM10 : ");
  Serial.print(pm_10);
  Serial.println(" ug/m3");
  Serial.println("==================");
  dust = false; // reset a boolean, for a new measurement
}

//========================================================
//  GPS-Receiver - EM506
//========================================================

#define GPS_LEN 67    // Length of the data packet minus the start byte and message ID
#define GPS_ID_LEN 6  // Length of Message ID
// Char, in which the output of the GPS receiver is temporarily saved
unsigned char gps_buf[GPS_LEN];
// Char, in which Message ID from the Output is temporarily stored for checking
unsigned char gps_id_buf[GPS_ID_LEN];
// Boolean to indicate whether data has been read or not
bool gps = false;
// Boolean to indicate a GPS Receiver state(fixed or not)
bool gps_fixed = false;

unsigned char gps_incomingByte = 0;

String pdegree = "00";       // Degrees of latitude
String pminutes = "00.0000"; // minutes of latitude
String pole = "N";           // pole of latitude
String hdegree = "000";      // Degree of length
String hminutes = "00.0000"; // minutes of longitude
String hemi = "W";           // Degree of length
String sat = "00";           // Number of satellites found

/**  gps_read_data
 * -------------------------------------------------------
 * Overview: Function for recognizing the message ID and 
 * read data from sensor 
 */

void gps_read_data() {
  // A byte from the sensor is read
  gps_incomingByte = GPS_Serial.read();
  // If this byte corresponds to the Message Header $, ...
  if (gps_incomingByte == 0x24) {
    // ... the bytes of the Message ID are read out
    GPS_Serial.readBytes(gps_buf, GPS_ID_LEN);
    // Check on the GGA header
    if (gps_buf[2] == 0x47 && 
        gps_buf[3] == 0x47 &&
        gps_buf[4] == 0x41 )
    {
      // The remaining bytes of the data packet are read out
      GPS_Serial.readBytes(gps_buf, GPS_LEN);
      gps = true;   // Data was read  
    }
  }
}

/**  gps_check_position
 * -------------------------------------------------------
 * Overview: Function to check if a GPS signal was found
 */

void gps_check_position () {
   gps = false; // Reset boolean
   // If GPS receiver has a position, the boolean is set to true
   if (gps_buf[36] == 0x31 || gps_buf[36] == 0x32) gps_fixed = true;
   else {
     gps_fixed = false;
   }
   gps_gga_print (); // Print a data
}

/**  gps_gga_print
 * -------------------------------------------------------
 * Overview: Function to print the GPS data in the serial 
 * monitor
 */

void gps_gga_print () {
  Serial.println ("");
  Serial.println ("===================");
  Serial.println ("");

  // Format the time correctly
  if (gps_buf[0] != 0x2c) {
    for (i = 0; i<6; i++) {
      Serial.write (gps_buf[i]);
      if (i == 1 || i == 3) Serial.print (":");
    }
    Serial.println ("UTC");
  } else {
    Serial.println ("Error: No time zone found or incorrect");
  }

  // if a position was found, coordinates will be printed in the serial monitor 
  if (gps_fixed) {
    // Read data from the char array
    for (i = 11; i <= 39; i++) {
      if (i == 11 || i == 12) pdegree.setCharAt (i - 11, gps_buf[i]);
      if (i >= 13 && i <= 19) pminutes.setCharAt (i - 13, gps_buf[i]);
      if (i == 21) pole.setCharAt (0, gps_buf[i]);
      if (i >= 23 && i <= 25) hdegree.setCharAt (i - 23, gps_buf[i]);
      if (i >= 26 && i <= 32) hminutes.setCharAt (i - 26, gps_buf[i]);
      if (i == 34) hemi.setCharAt (0, gps_buf[i]);
      if (i == 38 || i == 39) sat.setCharAt (i - 38, gps_buf[i]);
    }

    // Print data from the char array in the serial monitor
    Serial.print (pdegree);
    Serial.print ("");
    Serial.print (pminutes);
    Serial.print ("");
    Serial.print (pole);
    Serial.print ("");
    Serial.print (hdegree);
    Serial.print ("");
    Serial.print (hminutes);
    Serial.print ("");
    Serial.println (hemi);

    // Number of GPS satellites found by the GPS receiver
    Serial.print ("Satellites:");
    Serial.println (sat);

  }
  else {
    Serial.println ("Error: position cannot be determined");
  }
}


//========================================================
//  Bluetooth
//========================================================

// importing the necessary libraries for the BLE capabilities
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL; 
bool deviceConnected = false; //BT state variable
bool oldDeviceConnected = false; //variable for reconnect with old device

// Service and Characteristic UUID for Bluetooth Device 
#define SERVICE_UUID        "959009bc-b6e9-11ea-b3de-0242ac130004"
#define CHARACTERISTIC_UUID "a0ee616e-b6e9-11ea-b3de-0242ac130004"

class MyServerCallbacks: public BLEServerCallbacks {
/**  onConnect
 * -------------------------------------------------------
 * Overview: Function which will be called, when BT Device 
 * is connecting
 */
    void onConnect(BLEServer* pServer) {
      analogWrite(PIN_CONTROL_DUST, 255); //turn a DUST Sensor "ON"
      deviceConnected = true;
      Serial.println("Device is connected");
      dust_send_work_cmd(); // Send a WORKCMD to DUST Sensor
      rgb_write(0,255,0); // Set RGB to green
    }

/**  onDisconnect
 * -------------------------------------------------------
 * Overview: Function which will be called, when BT Device 
 * is disconnecting
 */
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("Device is disconnected");
      rgb_write(255,0,0); // Set RGB to red   
    }
};

//========================================================
//  Setup block
//========================================================

void setup() {

  //===============
  //  ESP32
  //===============
  
  // It starts the serial communication at a baud rate of 9600
  Serial.begin(9600);

  // It starts the serial communication with a GPS Sensor 
  //at a baud rate of 4800
  GPS_Serial.begin(4800, SERIAL_8N1, RXD1, TXD1);
  Serial.println("Serial Txd1 is on pin: " + String(TXD1));
  Serial.println("Serial Rxd1 is on pin: " + String(RXD1));
  // It starts the serial communication with a DUST Sensor 
  // at a baud rate of 9600
  Dust_Serial.begin(9600, SERIAL_8N1, RXD2, TXD2);
  Serial.println("Serial Txd2 is on pin: " + String(TXD2));
  Serial.println("Serial Rxd2 is on pin: " + String(RXD2));


  // Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  // enable an external wake up on ESP32 
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1); //1 = High, 0 = Low

  // enable a control pins for MOSFETS
  pinMode(PIN_CONTROL_GPS, OUTPUT);
  pinMode(PIN_CONTROL_DUST, OUTPUT);
  // turn the sensors on
  analogWrite(PIN_CONTROL_GPS, 255);
  analogWrite(PIN_CONTROL_DUST, 255);

  //===============
  //  BLE
  //===============

  // Create a BLE device called “GeliOS”
  BLEDevice::init("GELIOS");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
  Serial.println(SERVICE_UUID);
  Serial.println(CHARACTERISTIC_UUID);
  //set a REGB LED to ltblue
  rgb_write(255,255,125);
}

//========================================================
//  Loop block
//========================================================
void loop() {
  if (deviceConnected) {

    // Collect data

    // esp32 starts with collecting a dust data
    if (!dust && !gps) {
      // send a DATACMD to dust sensor
      for (uint8_t i = 0; i < 19; i++) {
        Dust_Serial.write(DATACMD[i]);
        Dust_Serial.flush();
      }
      // collect dust data if dust is available
      if (Dust_Serial.available() > 0) {
        dust_read_data();
      }
    }
    // after a successful read of dust data, esp32 will read
    // a gps position
    if (!gps && dust) {
      if (GPS_Serial.available() > 0) {
        gps_read_data();
        if (gps) {
          Serial.println("GPS data ready");
        }
      }
    }
    // after esp32 collected all the data,
    // esp32 starts to advertise Data
    if (dust && gps) {
      // prepare data to print
      gps_check_position();
      dust_print();
      //mesure a loudness data
      loudness_measure();
      // make a json string with measured data
      String json_data = "{\"pdegree\":" + String(" \" ") + String(pdegree) + String(" \", ") +
                         "\"pminutes\":" + String(" \" ") + String(pminutes) + String(" \", ") +
                         "\"pole\":" + String(" \" ") + String(pole) + String(" \", ") +
                         "\"hdegree\":" + String(" \" ") + String(hdegree) + String(" \", ") +
                         "\"hminutes\":" + String(" \" ") + String(hminutes) + String(" \", ") +
                         "\"hemisphere\":" + String(" \" ") + String(hemi) + String(" \", ") +
                         "\"mic\":" + String(" \" ") + String(loudness) + String(" \", ") +
                         "\"pm10\":" + String(" \" ") + String(pm_10) + String(" \", ") +
                         "\"pm25\":" + String(" \" ") + String(pm_2_5) + String(" \", ") +
                         "\"bat\":" + String(" \" ") + String(battery_r) + String(" \" ") +
                         String("}");
      Serial.println(json_data);
      // send a json string to BT, BT starts advertisnig data
      pCharacteristic->setValue(json_data.c_str());
      pCharacteristic->notify();
      delay(10); 
      Serial.println("JSON sent");
      sleep_counter = 0;
    }
  }
  else {
    // check how long esp32 is sleeping
    // when counter greather than 400
    if (sleep_counter >= 400) {
      // and dust sensor doesn't sleep now
      if (!dust_sleep_state) {
        // cut a power to a dust sensor
        analogWrite(PIN_CONTROL_DUST, 0);
        // set a sleep variable to true
        dust_sleep_state = true;
        Serial.println(dust_sleep_state);
        Serial.println("dust sensor going to sleep now");
      }
      // and dust is sleeping now
      else {
        // when "shut down" counter is reached
        if (sleep_counter == 800) {
          sleep_counter = 0;
          // cut a power to a dust and gps sensor
          analogWrite(PIN_CONTROL_GPS, 0);
          analogWrite(PIN_CONTROL_DUST, 0);
          // turn the LED off
          rgb_write(0,0,0);
          // esp deep sleep command
          esp_deep_sleep_start();
        }
        // when "shut down" counter isn't reached, counter +1
        else {
          sleep_counter += 1;
          Serial.println("Attemp to reconnect: " + String(sleep_counter) + " of 800");
        }
      }
    }
    // when "shut down" counter isn't reached, counter +1
    else {
      sleep_counter += 1;
      Serial.println("Attemp to reconnect: " + String(sleep_counter) + " of 800");
    }
  }
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
