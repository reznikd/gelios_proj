#include <Arduino.h>
#include <analogWrite.h>


#define PIN_CONTROL_DUST 5 // GPIO5
#define PIN_CONTROL_GPS 18 // GPIO18

int brightStep = 1;
int brightness = 255;
int red_light_pin= 39;

void setup() {
  // Set resolution for a specific pin
  pinMode(PIN_CONTROL_GPS, OUTPUT);
  pinMode(PIN_CONTROL_DUST, OUTPUT);
}

void loop() {
  analogWrite(PIN_CONTROL_GPS, 255);
  analogWrite(PIN_CONTROL_DUST, 255);
  delay(10000);
  analogWrite(PIN_CONTROL_GPS, 0);
  analogWrite(PIN_CONTROL_DUST, 0);
  delay(10000);
}
