# GeliOS

GeliOS System searches for the "Most comfortable way" between two points on the map based on some server collected Data from users. Furthermore collect the system telemetry Data from users. Information to collect:

- Location of ESP32 Device
- Air condition data
- Road condition data
- Noise pollution data

GeliOS is the System of 3 Applications:

1. Mobile Application for [more [here](MobileApp "MobileApp")]:
   - Search for "Best path" between two points on the map (Mabye Google API)
   - Sending collected from ESP32 telemetry Data from user (ESP32 Hardware with ESP32App needed)
2. Server Application for [more [here](ServerApp "ServerApp")]
   - Receiving telemetry Data from users
   - Saving and Processing collected Data
   - Communication with MobileApp for building of Route
   - (*Optional*) Internet Site for interact with system features from PC
3. Software Solution for ESP32App Devices [more [here](ESP32App "ESP32App")]
   - Collecting telemetry Data from sensors
   - Sending collected Data to mobile Device per Bluetooth



#### Authors:

**Ivan Reichert**: [email](mailto:iv_re@uni-bremen.de)

**Dmytro Reznik**: [email](mailto:dmytro@uni-bremen.de)
